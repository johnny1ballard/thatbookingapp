import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  //{ path: '', redirectTo: 'pages/dashboard', pathMatch: 'full',canActivate:[AuthGuardService] },
  //{ path: 'dashboard', redirectTo: 'pages/dashboard', pathMatch: 'full',canActivate:[AuthGuardService] },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'pages',
    loadChildren: () => import('./pages/pages.module').then( m => m.PagesPageModule),canActivate: [AuthGuardService]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules,enableTracing:false})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
