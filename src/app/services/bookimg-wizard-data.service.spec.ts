import { TestBed } from '@angular/core/testing';

import { BookimgWizardDataService } from './bookimg-wizard-data.service';

describe('BookimgWizardDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookimgWizardDataService = TestBed.get(BookimgWizardDataService);
    expect(service).toBeTruthy();
  });
});
