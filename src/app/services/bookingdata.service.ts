import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookingdataService 
{

  private data:any=[] ;
  constructor() { }

  /*setData(id, data) {
    this.data[id] = data;
  }

  getData(id) 
  {
    return this.data[id];
  }
*/
  setData(data) {
    this.data = data;
  }

  getData() 
  {
    return this.data;
  }
 
  
}
