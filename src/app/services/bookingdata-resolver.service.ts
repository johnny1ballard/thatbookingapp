import { Injectable } from '@angular/core';
import { BookingPageRoutingModule } from '../pages/booking/booking-routing.module';
import { BookingdataService } from './bookingdata.service';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BookingdataResolverService implements Resolve<any>
{

  constructor(private bookingData: BookingdataService)
   { }

   resolve(route: ActivatedRouteSnapshot) {
    //let id = route.paramMap.get('id');
    return this.bookingData.getData();
  }
}
