import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()
export class GlobalProvider {
  public userId: string;
  public config: any;
  public label: any;
  public svgIcons: any;
  public fcmToken: any;
  public packageName: any;
  public devicePlatform: any;

  constructor(public domSanitizer: DomSanitizer) {
    this.label = this.getLabel();
    this.svgIcons = this.getSvgIcos();
    //this.config = this.getConfig(); //veramatic
    
  }

  getLabel() {
    return {
      app: {
        appName: 'That Booking',
        noInternet: 'No internet connection !',
        message: 'Please check your internet connection and try again.',
        retryBtn: 'Retry'
      },
      login: {
        title1: 'Log in to get a personalized',
        title2: 'experience & quick checkout',
        continue: 'CONTINUE WITH',
        username: 'Email',
        password: 'Password',
        login: 'Log In',
        forgot: 'FORGOT PASSWORD ?',
        new: 'New to ThatBooking ?',
        signup: 'Signup',
        now: 'now',
        dashboard: 'Loading Dashboard ...',
        errMsg: 'Error 404: Login Failed',
        passwordResetText: 'Your password reset successfully',
        appinfo: 'APP INFO',
      },
      signup: {
        title: 'SIGN-UP',
        email: 'Email address',
        fullName: 'Full Name',
        username: 'User Name',
        password: 'Password',
        genderF: 'Female',
        genderM: 'Male',
        terms: 'I have accept T & C',
        msgTerms: 'Please accept T&C',
        create: 'Create Account',
        userRegistration: 'Registering user..',
        readMore: 'Read more ...'
      },
      dashboard: {
        welcome: 'Welcome',
        confirmLogout: 'Sure you want to logout ?',
        userServices: 'Services',
        userBooking: 'Make a Booking',
        userMyBooking: 'My Booking',
        userMyProfile: 'My Profile',
        userContact: 'Contact',
        empolyeeViewAppointments: 'View Appointments',
        empolyeeMyAppointments: 'My Appointments',
        empolyeeProfile: 'Profile',
        info: 'Info',
        updateProfile: 'Update profile !'
      },
      booking: {
        title: 'BOOK APPOINTMENT',
        subtitle1: 'appointment details',
        service: 'Select service',
        serviceEmpty: 'Service name',
        date: 'Select date',
        therapist: 'Select therapist',
        view: 'View',
        time: 'Select time',
        timeEmpty: 'Time',
        cost: 'Cost',
        note: 'Comments for treatment provider',
        patient: 'Patient',
        bookOther: 'Book for other',
        name: 'Name',
        email: 'Email',
        phoneNumber: 'Phone number',
        address: 'Address',
        town: 'Town',
        postCode: 'Post code',
        submitBtn: 'Book',
        subject: 'Subject',
        calendarDate: 'Select Date',
        closeLabel: 'Cancel',
        doneLabel: 'Select',
        msgServiceName: 'Please select service',
        msgBookingDate: 'Please select booking date',
        msgSelectTime: 'Please select time',
        msgBookingTotal: 'Please set charges',
        msgCname: 'Please select name',
        msgCemail: 'Please select email',
        msgCphoneNo: 'Please select phone number',
        msgCaddress: 'Please select address',
        msgCcity: 'Please select city',
        msgCzip: 'Please select zip code',
        msgTherapist: 'Please select therapist',
        msgService: 'Please select service',
        msgConfirm: 'Sure you want to book appointment ?',
        msgSuccess: 'Appointment Booked successfully!',
        msgError: 'Error 404: Booking Failed',
        msgPaymentSuccess: 'Payment done successfully.',
        msgPaymentError: 'Payment error !'
      },
      tabThree: {
        title1: 'VIEW APPOINTMENTS',
        title2: 'MY BOOKING',
        tab1: 'All',
        tab2: 'Upcoming',
        tab3: 'Past',
      },
      therapist: {
        view: 'View',
        book: 'Book',
        send: 'Message',
        pageName: 'Treatment Provider',
        getTherapists: 'Getting therapists..',
        back: 'Back'
      },
      contact: {
        title: 'CONTACT',
        subject: 'SUBJECT',
        message: 'TYPE YOUR MESSAGE',
        submitBtn: 'Send',
        view: 'View',
        msgSubject: 'Please enter Subject',
        msgMessage: 'Please enter Message',
        msgTherapist: 'Please select therapist',
        msgConfirm: 'Sure you want to send message ?',
        msgSuccess: 'Message sent successully !'
      },
      profile: {
        name: 'Name',
        email: 'Email',
        phone: 'Phone',
        city: 'City',
        state: 'State',
        address: 'Address',
        country: 'Country',
        postcode: 'Post Code',
        submitBtn: 'Submit',
        updateProfile: 'UPDATE PROFILE',
        information: 'INFORMATION',
        error: 'Error 404',
        profileupdateerr: 'Profile update failed !',
        pleasewait: 'Please wait',
        confirmMesg: 'Sure you want to save changes ?',
      },
      appointment: {
        title: 'Appointement Details',
        filter: 'Filter'
      },
      blockOut: {
        title: 'My Appointments',
        details: 'Details',
        emptyAttendees: '0 attendees !',
      },
      country: {
        title: 'Select Country',
        btnClose: 'Close',
        btnDone: 'Done',
      },
      empty: {
        msgBooking: 'No Booking History...',
        msgBlockOut: 'No Block Out !!!',
        msgError: 'Error 404!',
        msgTherapist: 'THERAPIST NOT FOUND',
        subTitleTherapist: 'Please select service',
        msgSelectTherapist: 'SELECT THERAPIST',
        subTitleSelectTherapist: 'THERAPIST DETAILS',
        msgService: 'SERVICE NOT FOUND',
        subTitleService: 'SERVICE LIST EMPTY',
      },
      servicesModal: {
        error: 'Error 404',
        serviceMsg: 'NO SERVICE IS AVAILABLE NOW !!!',
        services: 'SERVICES',
        details: 'Details',
        servicePrc: 'Service Price:',
        back: 'Back',
      },
      serviceTimeslots: {
        getTimemsg: 'Getting timeslots..',
        unavailableslots: 'UNAVAILABLE TIME SLOTS',
        availableTime: 'AVAILABLE TIME SLOTS',
        select: 'Select',
        cancel: 'cancel',
        bookTime: 'Book appointment time',
        timeNotAvailable: 'Therapist time slots are not available'
      },
      mybooking: {
        errMsg: 'No record found !',
        status: 'Status',
        scheduleConfirm: 'Schedule - Confirmed',
        pending: 'Pending',
        canceld: 'Canceled',
        fee: 'Fees',
        bookfor: 'Book For',
        time: 'Time',
        slot: 'Slot',
      },
      info: {
        info: {
          title: 'INFO',
          keyName: 'about'
        },
        terms: {
          title: 'Terms & conditions',
          keyName: 'tearm'
        }
      },
      success: {
        failed: 'PAYMENT FAILED !',
        tab1: 'BOOKING DETAILS',
        btn1: 'RETRY',
        success: 'PAYMENT SUCCESSFUL',
        tab2: 'BOOKING RECEIPT',
        btn2: 'PAYMENT COMPLETED'
      }
    };
  }

  getSvgIcos() {
    let obj = {
      list: {
        data: this.getTrustHtml(`
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
            class="feather feather-list"
          >
            <line x1="8" y1="6" x2="21" y2="6"></line>
            <line x1="8" y1="12" x2="21" y2="12"></line>
            <line x1="8" y1="18" x2="21" y2="18"></line>
            <line x1="3" y1="6" x2="3" y2="6"></line>
            <line x1="3" y1="12" x2="3" y2="12"></line>
            <line x1="3" y1="18" x2="3" y2="18"></line>
          </svg>
        `)
      },
      calendar: {
        data: this.getTrustHtml(`
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
            class="feather feather-calendar"
          >
            <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
            <line x1="16" y1="2" x2="16" y2="6"></line>
            <line x1="8" y1="2" x2="8" y2="6"></line>
            <line x1="3" y1="10" x2="21" y2="10"></line>
          </svg>
        `)
      },
      home: {
        data: this.getTrustHtml(`
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
            class="feather feather-home"
          >
            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
            <polyline points="9 22 9 12 15 12 15 22"></polyline>
          </svg>
        `)
      },
      setting: {
        data: this.getTrustHtml(`
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            fill="none"
            stroke="currentColor"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width="2"
            class="feather feather-settings"
            viewBox="0 0 24 24"
          >
            <circle cx="12" cy="12" r="3" />
            <path
              d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"
            />
          </svg>
        `)
      },
      user: {
        data: this.getTrustHtml(`
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
            class="feather feather-user"
          >
            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
            <circle cx="12" cy="7" r="4"></circle>
          </svg>
        `)
      },
      info: {
        data: this.getTrustHtml(`
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
            class="feather feather-info"
          >
            <circle cx="12" cy="12" r="10"></circle>
            <line x1="12" y1="16" x2="12" y2="12"></line>
            <line x1="12" y1="8" x2="12" y2="8"></line>
          </svg>
        `)
      },
      analytics: {
        type: 'fill',
        data: this.getTrustHtml(
          `
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="24" height="24">
            <path d="M379.4 178.3l-87.2 133.4C299 320 303 330.5 303 342c0 26.5-21.5 48-48 48s-48-21.5-48-48c0-3 .3-6 .8-8.9l-57.6-33.5c-8.6 8.3-20.3 13.4-33.3 13.4-8.6 0-16.6-2.3-23.6-6.2L32 364.2v57.2c0 23.5 19.2 42.7 42.7 42.7h362.7c23.5 0 42.7-19.2 42.7-42.7V208.8l-58.6-38.9c-8.1 6.3-18.3 10.1-29.4 10.1-4.4 0-8.7-.6-12.7-1.7z"/>
            <path d="M117 217c26.5 0 48 21.5 48 48 0 2.1-.2 4.2-.4 6.2l60.1 33.6c8.3-6.8 18.8-10.8 30.4-10.8 3.6 0 7.1.4 10.4 1.1l87.4-135.4c-5.6-7.8-8.9-17.4-8.9-27.8 0-26.5 21.5-48 48-48s48 21.5 48 48c0 3.9-.5 7.7-1.3 11.3l41.3 27.6V90.7c0-23.5-19.2-42.7-42.7-42.7H74.7C51.2 48 32 67.2 32 90.7V320l40-38.3c-1.9-5.2-3-10.8-3-16.7 0-26.5 21.5-48 48-48z"/>
          </svg>
        `)
      },
      doctor: {
        data: this.getTrustHtml(
          `
          <?xml version="1.0" encoding="iso-8859-1"?>
          <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 500.004 500.004" style="enable-background:new 0 0 500.004 500.004;" xml:space="preserve" class="svgcl">
          <g>
            <g>
              <path d="M250,0c-53.876,0-97.696,43.832-97.696,97.708v50.62c0,53.88,43.82,97.712,97.696,97.712
                c53.88,0,97.696-43.832,97.696-97.712v-50.62C347.697,43.832,303.88,0,250,0z"/>
            </g>
          </g>
          <g>
            <g>
              <path d="M465.456,373.364c-31.284-53.312-81.288-92.836-140.8-111.292c-1.38-0.428-2.872-0.064-3.9,0.952
                c-27.684,27.296-62.084,48.592-70.684,53.736c-9.18-5.868-47.848-31.072-70.836-53.736c-1.024-1.016-2.528-1.38-3.9-0.952
                c-59.52,18.46-109.52,57.984-140.792,111.296c-0.716,1.22-0.716,2.736,0,3.956c44.408,75.672,126.964,122.68,215.452,122.68
                c88.492,0,171.052-47.008,215.46-122.68C466.176,376.1,466.176,374.584,465.456,373.364z M382.872,402.508
                c0,2.156-2.052,3.804-4.204,3.804h-26.46c-2.156,0-4.508,1.852-4.508,4.012v27.364c0,2.152-1.152,3.796-3.308,3.796h-23.764
                c-2.156,0-4.188-1.644-4.188-3.796v-27.364c-0.008-2.156-1.48-4.012-3.636-4.012h-27.6c-2.156,0-3.94-1.652-3.94-3.804v-23.56
                c0-2.156,1.784-3.896,3.94-3.896h27.6c2.156,0,3.628-1.76,3.628-3.92v-27.396c0-2.156,2.032-3.856,4.188-3.856h23.508
                c2.152,0,3.568,1.7,3.568,3.856v27.476c0,2.16,2.088,3.836,4.248,3.836h26.724c2.152,0,4.204,1.824,4.204,3.98V402.508z"/>
            </g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g>
          </g>
          </svg>
        `)
      },
      wifi: {
        type: 'fill',
        data: this.getTrustHtml(
          `
            <?xml version="1.0" encoding="UTF-8"?>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              xmlns:xlink="http://www.w3.org/1999/xlink"
              width="500pt"
              height="500pt"
              version="1.1"
              viewBox="0 0 500 500"
            >
              <g id="surface1">
                <path
                  d="M 250 104.167969 C 175.25 104.167969 106.628906 130.421875 52.855469 174.195312 C 39.460938 185.089844 38.695312 205.363281 50.902344 217.570312 C 61.070312 227.738281 77.164062 229.042969 88.664062 220.457031 C 133.746094 186.75 189.5 166.667969 250 166.667969 C 310.5 166.667969 366.253906 186.769531 411.335938 220.457031 C 422.855469 229.0625 438.929688 227.695312 449.097656 217.527344 C 461.304688 205.320312 460.519531 185.050781 447.144531 174.152344 C 393.371094 130.402344 324.75 104.167969 250 104.167969 Z M 250 208.332031 C 206.082031 208.332031 165.101562 220.871094 130.25 242.433594 C 113.082031 253.035156 110.242188 276.90625 124.511719 291.179688 C 134.867188 301.53125 150.828125 302.914062 163.332031 295.246094 C 188.601562 279.769531 218.25 270.832031 250 270.832031 C 281.875 270.832031 311.660156 279.867188 336.996094 295.449219 C 349.285156 303.011719 365.035156 301.628906 375.246094 291.421875 L 375.8125 290.851562 C 389.980469 276.6875 387.15625 253.214844 370.28125 242.714844 C 335.300781 220.988281 294.125 208.332031 250 208.332031 Z M 250 312.5 C 231.125 312.5 213.265625 316.738281 197.226562 324.300781 C 184.140625 330.445312 181.136719 347.804688 191.367188 358.03125 L 228.476562 395.140625 C 240.371094 407.039062 259.628906 407.039062 271.523438 395.140625 L 308.632812 358.03125 C 318.863281 347.804688 315.859375 330.445312 302.773438 324.300781 C 286.734375 316.738281 268.875 312.5 250 312.5 Z M 372.476562 351.644531 C 367.144531 351.644531 361.820312 353.675781 357.746094 357.746094 C 349.621094 365.871094 349.621094 379.0625 357.746094 387.207031 L 387.207031 416.667969 L 357.746094 446.125 C 349.621094 454.273438 349.621094 467.460938 357.746094 475.585938 C 365.871094 483.710938 379.082031 483.710938 387.207031 475.585938 L 416.667969 446.125 L 446.125 475.585938 C 454.25 483.710938 467.460938 483.710938 475.585938 475.585938 C 483.710938 467.460938 483.710938 454.25 475.585938 446.125 L 446.125 416.667969 L 475.585938 387.207031 C 483.710938 379.082031 483.710938 365.871094 475.585938 357.746094 C 467.460938 349.621094 454.25 349.621094 446.125 357.746094 L 416.667969 387.207031 L 387.207031 357.746094 C 383.144531 353.675781 377.808594 351.644531 372.476562 351.644531 Z M 372.476562 351.644531 "
                  style="stroke:none;fill-rule:nonzero;fill-opacity:1"
                />
              </g>
            </svg>
          `
        )
      },
      error: {
        data: this.getTrustHtml(
          `
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="Capa_1" x="0px" y="0px" style="enable-background:new 0 0 511.999 511.999" version="1.1" viewBox="0 0 511.999 511.999" xml:space="preserve">
            <path d="M501.449,368.914L320.566,66.207C306.751,43.384,282.728,29.569,256,29.569
          s-50.752,13.815-64.567,36.638L10.55,368.914c-13.812,23.725-14.113,51.954-0.599,75.678c13.513,23.723,37.836,37.838,65.165,37.838
          h361.766c27.329,0,51.653-14.115,65.165-37.838C515.563,420.868,515.262,392.639,501.449,368.914z" style="fill:#E50027"/>
            <path d="M502.049,444.592c-13.513,23.723-37.836,37.838-65.165,37.838H256V29.57
          c26.727,0,50.752,13.815,64.567,36.638L501.45,368.915C515.262,392.639,515.563,420.868,502.049,444.592z" style="fill:#C1001F"/>
            <path d="M75.109,452.4c-16.628,0-30.851-8.27-39.063-22.669c-8.211-14.414-8.065-31.087,0.469-45.72
          L217.23,81.549c8.27-13.666,22.816-21.951,38.769-21.951s30.5,8.284,38.887,22.157l180.745,302.49
          c8.388,14.4,8.534,31.072,0.322,45.485c-8.211,14.4-22.435,22.669-39.063,22.669H75.109V452.4z" style="fill:#FD003A"/>
            <path d="M436.891,452.4c16.628,0,30.851-8.27,39.063-22.669c8.211-14.414,8.065-31.087-0.322-45.485
          L294.886,81.754c-8.388-13.871-22.933-22.157-38.887-22.157V452.4H436.891z" style="fill:#E50027"/>
            <path d="M286.03,152.095v120.122c0,16.517-13.514,30.03-30.03,30.03s-30.031-13.514-30.031-30.03V152.095
          c0-16.517,13.514-30.031,30.031-30.031S286.03,135.578,286.03,152.095z" style="fill:#E1E4FB"/>
            <path d="M286.03,152.095v120.122c0,16.517-13.514,30.03-30.03,30.03V122.064
          C272.516,122.064,286.03,135.578,286.03,152.095z" style="fill:#C5C9F7"/>
            <path d="M256,332.278c-24.926,0-45.046,20.119-45.046,45.046c0,24.924,20.119,45.046,45.046,45.046
          s45.046-20.121,45.046-45.046C301.046,352.398,280.925,332.278,256,332.278z" style="fill:#E1E4FB"/>
            <path d="M301.046,377.323c0,24.924-20.119,45.046-45.046,45.046v-90.091
          C280.925,332.278,301.046,352.398,301.046,377.323z" style="fill:#C5C9F7"/>
          </svg>
        `)
      },
      success: {
        data: this.getTrustHtml(
          `
          <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" class="svg-inline--fa fa-check-circle fa-w-16" data-icon="check-circle" data-prefix="fas" focusable="false" role="img" viewBox="0 0 512 512">
              <path fill="rgba(41, 171, 135, 0.8)" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z"/>
          </svg>
        `)
      }
    };

    return obj;
  }

  getTrustHtml(url: any) {
    return this.domSanitizer.bypassSecurityTrustHtml(url);
  }

  /**
   * by veramatic for testing
   *
  getConfig()
  {
    return {"id":"1","app_name":"New App","package_name":"com.limedrive.tp408","ossb_category_id":"4","logo":"http:\/\/thatbooking.limedrive.co.uk\/thatbooking\/images\/TBA_logo.png","small_logo":"http:\/\/thatbooking.limedrive.co.uk\/thatbooking\/images\/TBA_logo_c.png","splash_screen":"http:\/\/thatbooking.limedrive.co.uk\/thatbooking\/images\/HeatherWax_Splash_1.jpg","bg_image_1":"images\/app\/app_background1.png","bg_image_2":"images\/app\/app_background2.png","about":"<h3>Why your store needs a<strong>\u00a0Mobile App?<\/strong><\/h3>\r\n<p>More than 60% of online shopping happens in mobile. Customers are increasingly using their smartphones to order products online. You do not want to be left behind in the mobile revolution.<\/p>\r\n<p>Having a mobile app for your store helps you retain your customers for the long term. Most of the customers who install a shopping app, keep it and purchase more. That means they can make repeat purchases, thus increasing your customer Life Time Value (LTV).<\/p>\r\n<h3>How That Booking App can help you and your customers<\/h3>\r\n<p>Fill your day with happy customers....<\/p>\r\n<p>\u00a0<\/p>","tearm":"<p><strong>Terms &amp; Conditions<\/strong><\/p>\r\n<p>Sample Terms &amp; Conditions Text\u00a0<\/p>","therapist":"408","start_time":"10:00:00","end_time":"19:00:00","off_day":"7","created_by":"408","modified_by":"399","fcm_api_key":"AAAA4Rbwj2U:APA91bFQY1NOci-3TAYiJY0jTyRiZmsE-_RIO8XWp_2Faauz_fg_YJaIrlQZRfAA622-lXlR7d8Qua71O2w_flC2X4qvlGVksJ-GOy2T0ST6whuQEzyFcyZoBnUqLLb7q5zDYwG1HR2z","fcm_icon":"google-services.json","play_store_key":"release-key.keystore","params":"","label":{"app":{"appName":"That Booking","noInternet":"No internet connection !","message":"Please check your internet connection and try again.","retryBtn":"Retry"},"login":{"title1":"Log in to That Booking App for ","title2":"a quick and easy booking","continue":"LOG IN","username":"Username","password":"Password","login":"Log In","forgot":"FORGOT PASSWORD ","new":"New to ThatBooking ?","sign_up":"SignUp","now":"now","dashboard":"Loading Dashboard ...","error_msg":"Error 404: Login Failed","passwordResetText":"Your password reset successfully","appinfo":"APP INFO"},"signup":{"title":"SIGN UP","email":"Email address","fullName":"Full Name","username":"Username","password":"Password","genderF":"Female","genderM":"Male","terms":"I have accept Terms and Conditions","msgterms":"Please accept T&C","create":"Create Account","userRegistration":"Registering user..","readMore":"Read Terms & Conditions"},"dashboard":{"welcome":"Welcome ","confirmLogout":"Sure you want to logout ?","userServices":"Services","userBooking":"Make Booking","userMyBooking":"My Booking","userMyProfile":"My Profile","userContact":"Contact","empolyeeViewAppointments":"View Appointments","empolyeeMyAppointments":"My Appointments","empolyeeProfile":"Profile","info":"Info","updateprofile":"Update profile !"},"booking":{"title":"BOOK APPOINTMENT","subtitle1":"appointment details","service":"Select service:","service_empty":"Service name","date":"Select date","therapist":"Select therapist","view":"View","time":"Please select time","time_empty":"Time","cost":"Cost","note":"Comments for treatment provider","patient":"Patient","bookOther":"Book for other","name":"Name","email":"Email","phoneNumber":"Phone number","address":"Address","town":"TownTown","postCode":"Post code","submitBtn":"Book","subject":"Subject","calendarDate":"Select Date","closeLabel":"Cancle","doneLabel":"Select","msgServiceName":"Please select service","msgBookingDate":"Please select booking date","msgSelectTime":"Please select time","msgBookingTotal":"Please set charges","msgCname":"Please select name","msgCemail":"Please select email","msgCphoneNo":"Please select phone number","msgCaddress":"Please select address","msgCcity":"Please select city","msgCzip":"Please select zip code","msgTherapist":"Please select therapist","msgService":"Please select service","msgConfirm":"Sure you want to book appointment ?","msgSuccess":"Appointment Booked successfully!","msgError":"Error 404: Booking Failed","msgpaymentsuccess":"Payment done successfully.","msgpaymenterror":"Payment error !"},"tabThree":{"title1":"VIEW APPOINTMENTS","title2":"MY BOOKING","tab1":"All","tab2":"Upcoming","tab3":"Past"},"therapist":{"view":"View","book":"Book","send":"Message","pageName":"Treatment Provider","getTherapists":"Getting therapists..","back":"Back"},"contact":{"title":"CONTACT","subject":"SUBJECT","message":"TYPE YOUR MESSAGE","submitBtn":"Send","view":"View","msgSubject":"Please enter Subject","msgMessage":"Please enter Message","msgTherapist":"Please select Therapist","msgConfirm":"Sure you want to send message ?","msgSuccess":"Message sent successully !"},"profile":{"name":"Name:","email":"Email:","phone":"Phone :","city":"City","state":"State","address":"Address","country":"Country","postcode":"Postcode","submitBtn":"Submit","updateProfile":"UPDATE PROFILE","information":"Information","error":"Error 404","profileupdateerr":"Profile update failed !","pleasewait":"Please wait","confirmMesg":"Sure you want to save changes ?"},"appointment":{"title":"Appointment Details","filter":"Filter"},"blockOut":{"title":"My Appointments","details":"Details","emptyAttendees":"0 attendees !"},"country":{"title":"Select Country","btnClose":"Close","btnDone":"Done"},"empty":{"msgBooking":"No Booking History...","msgBlockOut":"No Block Out !!!","msgError":"Error 404!","msgTherapist":"THERAPIST NOT FOUND","subTitleTherapist":"Please select service","msgSelectTherapist":"SELECT THERAPIST","subTitleSelectTherapist":"THERAPIST DETAILS","msgService":"SERVICE NOT FOUND","SubTitleService":"SERVICE LIST EMPTY"},"servicesModal":{"error":"Error 404","serviceMsg":"NO SERVICE IS AVAILABLE NOW !!!","services":"SERVICES","details":"Details","servicePrc":"Service Price:","back":"Back"},"serviceTimeslots":{"getTimemsg":"Getting timeslots..","unavailableslots":"UNAVAILABLE TIME SLOTS","availableTime":"AVAILABLE TIME SLOTS","select":"Select","cancle":"Cancle","bookTime":"Book appointment time","timenotavailable":"Therapist time slots are not available"},"mybooking":{"errMsg":"No record found !","status":"Status","scheduleConfirm":"Schedule - Confirmed","pending":"Pending","canceld":"Canceld","fee":"Fees","bookfor":"Book For","time":"Time","slot":"Slot"},"info":{"info":{"title":"INFO","keyName":"about"},"terms":{"title":"Terms & conditions","keyName":"tearm"}},"success":{"failed":"PAYMENT FAILED !","tab1":"BOOKING DETAILS","btn1":"RETRY","success":"PAYMENT SUCCESSFUL","tab2":"BOOKING RECEIPT","btn2":"HOME"}},"forgot_pwd_url":"http:\/\/thatbooking.limedrive.co.uk\/thatbooking\/index.php?option=com_users&view=reset&tmpl=component","base_url":"http:\/\/thatbooking.limedrive.co.uk\/thatbooking\/","currency":"USD","currency_symbol":"$"} ;
    
  }

  getuserDate()
  {
    return JSON.stringify({
      "auth": "94cf4addab2a1afd2fd15828f9e3a7ad",
      "code": "200",
      "id": "427",
      "user_role": 0,
      "fcm_success": 11,
      "jwt": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjQyNyJ9.Xb-7vSdxyXj65TXqut-pVCnQEIi9I-ZHJABCeUeI8rY"
  } );

  }
  */


}
