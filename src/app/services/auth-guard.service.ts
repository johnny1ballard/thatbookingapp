import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { CommonProvider } from './common/common';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router,public common:CommonProvider) { }

  canActivate(route: ActivatedRouteSnapshot): boolean 
  {
    console.log(route);

    if(this.common.getUserData() != null && this.common.getUserData() != undefined)
    {
      return true;
    }
    else if(localStorage.getItem("usrLoggedIn")=="true"){
        return true;
    }
    else{
        this.router.navigate(["login"]);
        return false;
    }

    return true;
  }

}
