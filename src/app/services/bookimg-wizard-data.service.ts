import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookimgWizardDataService {

  public serviceData =new EventEmitter<any>();
  public therapistData =new EventEmitter<any>();
  public timeSlotData =new EventEmitter<any>();
  
  public serviceFlag =  new EventEmitter<boolean>();

  public TherapistFlag =  new EventEmitter<boolean>();

  public dateFlag =  new EventEmitter<boolean>();


  constructor() { }

  
}
