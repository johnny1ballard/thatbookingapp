import { TestBed } from '@angular/core/testing';

import { BookingdataResolverService } from './bookingdata-resolver.service';

describe('BookingdataResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookingdataResolverService = TestBed.get(BookingdataResolverService);
    expect(service).toBeTruthy();
  });
});
