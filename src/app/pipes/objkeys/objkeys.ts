import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ObjkeysPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'objkeys'
})
export class ObjkeysPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: any, args: any[] = null): any {
    let keys = [];
    for (let key in value) {
      if (value[key]) {
        keys.push(key);
      }
    }
    return keys;
  }
}
