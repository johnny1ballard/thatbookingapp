import { NgModule } from '@angular/core';
import { ObjkeysPipe } from './objkeys/objkeys';
@NgModule({
	declarations: [ObjkeysPipe],
	imports: [],
	exports: [ObjkeysPipe]
})
export class PipesModule {}
