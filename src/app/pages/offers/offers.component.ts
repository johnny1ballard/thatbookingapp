import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { CommonProvider } from 'src/app/services/common/common';
import { ActiveoffersComponent } from './activeoffers/activeoffers.component';
import { ExpiredoffersComponent } from './expiredoffers/expiredoffers.component';
import { Location } from '@angular/common';


@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss'],
})
export class OffersComponent implements OnInit {

  tab1root: any;
  tab2root: any;
  // tab3root: any;

  title: string;
  label: any;
  selectedTab: any;

  passedObj:any;

  
  constructor(public commonprovider: CommonProvider,    public route:ActivatedRoute,
    private router: Router,
    private modal:ModalController,
    public location:Location
    ) 
    { 
      this.label = this.commonprovider.getLabels().tabThree;

    }

  ngOnInit() 
  {
    let st = this.commonprovider.getUserLoginId();
    console.log("asdasdasd="+st);
    
    if (this.route.snapshot.data['special']) 
    {
      this.passedObj = this.route.snapshot.data['special'].state.userdata;
    }

      this.tab1root = ActiveoffersComponent; 
      this.tab2root = ExpiredoffersComponent;

      console.log("sdsadasd");
  }

  getPage(userRole: any) {
    return userRole
      ? ActiveoffersComponent
      : ExpiredoffersComponent;
  }

  goToBackUrl()
  {
    this.location.back();
  }

}
