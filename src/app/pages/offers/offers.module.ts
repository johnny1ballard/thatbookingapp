import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OffersRoutingModule } from './offers-routing.module';
import { OffersComponent } from './offers.component';
import { ActiveoffersComponent } from './activeoffers/activeoffers.component';
import { ExpiredoffersComponent } from './expiredoffers/expiredoffers.component';
import { FormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material';
import { SuperTabsModule } from '@ionic-super-tabs/angular';
import { IonicModule } from '@ionic/angular';



@NgModule({
  imports: [
    CommonModule,
    OffersRoutingModule,
    FormsModule,
    IonicModule,
    SuperTabsModule,
    MatStepperModule
  ],
  declarations: [OffersComponent,ActiveoffersComponent,ExpiredoffersComponent]
})
export class OffersModule { }
