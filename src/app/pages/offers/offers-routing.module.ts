import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActiveoffersComponent } from './activeoffers/activeoffers.component';
import { ExpiredoffersComponent } from './expiredoffers/expiredoffers.component';
import { OffersComponent } from './offers.component';


const routes: Routes = [
  {
    path: '',
    component: OffersComponent,
    //resolve:{special:BookingdataResolverService},
    children:[
      //{ path:'', redirectTo:'appointment', pathMatch:'full'},
      { path : 'activeoffrs' , component:ActiveoffersComponent},
      { path : 'expiredoffrs',component:ExpiredoffersComponent},
      //{ path : 'bookAppointment',component:BookAppointmentComponent},
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffersRoutingModule { }
