import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AppSettings } from 'src/app/services/app-settings';
import { BookimgWizardDataService } from 'src/app/services/bookimg-wizard-data.service';
import { CommonProvider } from 'src/app/services/common/common';

@Component({
  selector: 'app-activeoffers',
  templateUrl: './activeoffers.component.html',
  styleUrls: ['./activeoffers.component.scss'],
})
export class ActiveoffersComponent implements OnInit {

  @Input() id: string;

  
  serviceFlow: boolean = false;
  userData: any = {};
  coupons: any = [];
  isCoupon:boolean=false;
  servicemsg: any;
  selectedService: any = {
    id: ''
  };
  dataObj: any;
  config: any;
  label: any;

  public passedObj:any;

  public buttonText = "" ;
  public buttonIcon = "" ;

  //wizard related variable
  @Input() frmWizard:boolean = false ;
  
  constructor(    
    public appsettings: AppSettings,
    public commonprovider: CommonProvider,
    public route:ActivatedRoute,
    private router: Router,
    private modal:ModalController,
    public bookingwzrd:BookimgWizardDataService
  ) 
  { 
    this.label = this.commonprovider.getLabels().servicesModal; //take coupons

    this.label.coupons = "Active Coupons" ;

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation() !=null && this.router.getCurrentNavigation().extras.state) {
        this.passedObj = this.router.getCurrentNavigation().extras.state.userdata;
      }
    });

  }

  ngOnInit() 
  {
    let st = this.commonprovider.getUserLoginId();
    this.config = this.commonprovider.getConfig();
    this.dataInit();
    console.log("asdasdasd="+this.frmWizard);
  }

  ionViewWillLoad() {
    this.config = this.commonprovider.getConfig();
    this.dataInit();
  }

  dataInit() 
  {
    if(this.passedObj != undefined)
    {
      this.serviceFlow = this.passedObj.serviceFlow;

      this.passedObj.id ? (this.selectedService.id = this.passedObj.id): 'nothing';

    }

    this.userData = this.commonprovider.getUserData();
    //testdata

    //this.services = [{service_name:"testService",servicePrc:"tssts",service_price:12}];

    this.commonprovider.showLoader(this.appsettings.serviceLoader);
    this.commonprovider
      .get(this.appsettings.coupons + '?aid=' + this.config.id+'&uid='+this.userData.id+"&type=active")
      .subscribe(
        (response: any) => {
          if (!response.err_code && response.count != 0) {
            this.isCoupon = true;
            this.coupons = response.data.coupons;
            this.commonprovider.hideLoader();

          } else {
            this.commonprovider.hideLoader();
            this.commonprovider.showToast(
              response.err_msg || this.label.error,
              5000
            );
          }
          response.count
            ? ''
            : (this.servicemsg = this.label.serviceMsg);
        },
        err => {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err.message);
        }
      );
      
  }

  closeModal() {
    this.modal.dismiss();
  }

  selectCoupon(event: any, obj: any) {
    event.stopPropagation();
    this.selectedService = obj;
    this.dataObj = obj;

    let TIME_IN_MS = 250;
    setTimeout(() => {
      this.setService();
    }, TIME_IN_MS);
  }

  setService() {
    this.bookingwzrd.TherapistFlag.emit(false);
    if (this.serviceFlow) 
    {
      let obj = {
        serviceFlow: this.serviceFlow,
        userData: this.userData,
        therapistId: '',
        selectedService: this.selectedService
      };

      //this.navCtrl.push(TherapistPage, { data: obj });

      let navigationExtras: NavigationExtras = {
        state: {
          data: obj
        }
      };

      if(!this.frmWizard)
      {
        /** @veramatic 
         * since to protect backward compatibility to eneable modals nd componnet propogation ,this if is use 
         * the else will contain logic for form wizard
         */
        this.router.navigate(["pages","therapist"],navigationExtras);

      }
      else
      {
        //this logic is for the passing data by form wizard
        //use service to interreact
        //this.bookingwzrd.setServiceData({ data: obj });
        this.bookingwzrd.serviceData.emit({ data: obj });
      }
      
    } else {
      if(!this.frmWizard)
      {
        /** @veramatic 
         * since to protect backward compatibility to eneable modals nd componnet propogation ,this if is use 
         * the else will contain logic for form wizard
         */
        this.modal.dismiss(this.dataObj);
      }
      else
      {
        //this logic is for the passing data by form wizard
        //use service to interreact
        //this.bookingwzrd.setServiceData({ data: this.dataObj });
        this.bookingwzrd.serviceData.emit({ selectedService: this.dataObj });
      }
    }
  }

  /* viewDetail(event: any, str: any) {
    event.stopPropagation();
    var plainText = str.replace(/<[^>]*>/g, '');
    this.commonprovider.showToaster(plainText, '3500');
  } */

  viewDetail(event: any, item: any) {
    event.stopPropagation();

    let maxUse = item.max_user_use ;
    let tmp =  item.max_user_use - item.user_used;
    let title = 'Coupon Details';
    let remainigUsage = tmp+"";
    if(maxUse == 0)
    {
      maxUse = "Unlimited" ;
      remainigUsage = "Unlimited" ;
    }

    let usageMSg ="";
    if(Number(item.max_total_use) >0 && Number(item.total_used) >= Number(item.max_total_use))
    {
      usageMSg="<b>(** This coupon has reached it's usage limit, please contact us if you think this is in error)</b>";
    }
    let desc = "" ;

    if(item.coupon_description != null)
      desc = item.coupon_description ;


    let msg = '<div>'
      + '<b class="coup">Offer Name: </b> ' + item.coupon_name + '<br>'
      + '<b>Use Code: </b> '  + item.coupon_code + '<br>'
      + '<b>Description: </b> ' + desc + '<br>'
      + '<b>Total Uses:</b> ' + maxUse + '<br>'
      + '<b>Remaining Uses </b>: ' + remainigUsage + '<br>'
      + '<br>'
      + usageMSg 
      + '</div>';

    this.commonprovider.showAlert(title, msg);
  }


}
