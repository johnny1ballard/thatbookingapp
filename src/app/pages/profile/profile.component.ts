import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonProvider } from 'src/app/services/common/common';
import { AppSettings } from 'src/app/services/app-settings';
import { ModalController, NavParams } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  profile: any = {};
  employee_photo: string = 'assets/imgs/user.png';
  profileUpdate: FormGroup;
  userDataObj: any;
  label: any;
  isEdit: boolean = false;
  isEditFlag: boolean = false;
  countries: any;
  config: any = {
    small_logo: 'assets/imgs/yoga.jpg'
  };
  selectedCountry: any = {
    name: ''
  };

  tempProfile:any ;
  passedObj:any;

  @Input() type: any;


  constructor(private formBuilder: FormBuilder,
    public commonprovider: CommonProvider,
    public appsettings: AppSettings,
    public modal: ModalController,
    public route:ActivatedRoute,
    private router: Router,
    public location:Location
    ) 
    { 
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation() != null && this.router.getCurrentNavigation().extras.state) {
          this.passedObj = this.router.getCurrentNavigation().extras.state ; //to check if its modal or page
          this.tempProfile = this.router.getCurrentNavigation().extras.state.userdata;
        }
      });
  


    }
 
  ngOnInit() 
  {

    if(this.type != undefined)
    {
      this.tempProfile =  this.type.state.userdata ;
    }
    //let tempProfile = this.navParams.get('therapistProfile');
    let tempProfile = this.tempProfile.therapistProfile;
    console.log("this.tempProfile"+tempProfile);
    

    this.config = this.commonprovider.getConfig();
    this.label = this.commonprovider.getLabels().profile;

    this.isEdit = tempProfile.id == this.commonprovider.getUserLoginId()
      && !tempProfile.user_role;

    this.employee_photo = this.config.small_logo;
    this.profile = this.filterObject(tempProfile);

    if (this.isEdit) {
      this.initProfileUpdateForm();
    }
    // this.getCountryList();
  }

  initProfileUpdateForm() {
    this.profileUpdate = this.formBuilder.group({
      name: ['', Validators.compose([
        Validators.required,
        Validators.pattern("^[A-Za-z0-9 _!?@#$&()\\-`.+,/\]*[A-Za-z0-9!?@#$&()\\-`.+,/\][A-Za-z0-9 _!?@#$&()\\-`.+,/\]*$")
      ])],
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern("^[A-Za-z0-9 _!?@#$&()\\-`.+,/\]*[A-Za-z0-9!?@#$&()\\-`.+,/\][A-Za-z0-9 _!?@#$&()\\-`.+,/\]*$")
      ])],
      phone: ['', Validators.compose([
        Validators.required,
        Validators.pattern("^[A-Za-z0-9 _!?@#$&()\\-`.+,/\]*[A-Za-z0-9!?@#$&()\\-`.+,/\][A-Za-z0-9 _!?@#$&()\\-`.+,/\]*$")
      ])],
      country: ['', Validators.compose([
        Validators.required,
        Validators.pattern("^[A-Za-z0-9 _!?@#$&()\\-`.+,/\]*[A-Za-z0-9!?@#$&()\\-`.+,/\][A-Za-z0-9 _!?@#$&()\\-`.+,/\]*$")
      ])],
      state: ['', Validators.compose([
        Validators.required,
        Validators.pattern("^[A-Za-z0-9 _!?@#$&()\\-`.+,/\]*[A-Za-z0-9!?@#$&()\\-`.+,/\][A-Za-z0-9 _!?@#$&()\\-`.+,/\]*$")
      ])],
      city: ['', Validators.compose([
        Validators.required,
        Validators.pattern("^[A-Za-z0-9 _!?@#$&()\\-`.+,/\]*[A-Za-z0-9!?@#$&()\\-`.+,/\][A-Za-z0-9 _!?@#$&()\\-`.+,/\]*$")
      ])],
      address: ['', Validators.compose([
        Validators.required,
        Validators.pattern("^[A-Za-z0-9 _!?@#$&()\\-`.+,/\]*[A-Za-z0-9!?@#$&()\\-`.+,/\][A-Za-z0-9 _!?@#$&()\\-`.+,/\]*$")
      ])],
      zip: ['', Validators.compose([
        Validators.required,
        Validators.pattern("^[A-Za-z0-9 _!?@#$&()\\-`.+,/\]*[A-Za-z0-9!?@#$&()\\-`.+,/\][A-Za-z0-9 _!?@#$&()\\-`.+,/\]*$")
      ])]
    });

    //this.profileUpdate.get('email').disable();
    this.profileEdit();
  }

  filterObject(tempProfile) {
    let profile = {};

    if (tempProfile.employee_name) {
      this.employee_photo = tempProfile.employee_photo;

      profile['name'] = tempProfile.employee_name;
      profile['email'] = tempProfile.employee_email;
      profile['phone'] = tempProfile.employee_phone;
      profile['about'] = tempProfile.employee_notes;

      // profile['country'] = tempProfile.user_info.order_country;
      // profile['state'] = tempProfile.user_info.order_state;
      // profile['city'] = tempProfile.user_info.order_city;
      // profile['address'] = tempProfile.user_info.order_address;
    } else {
      profile['name'] = tempProfile.name;
      profile['email'] = tempProfile.email;
      profile['phone'] = tempProfile.user_info.order_phone;
      if (!tempProfile.user_role) {
        profile['username'] = tempProfile.username;
        profile['country'] = tempProfile.user_info.order_country;
        profile['state'] = tempProfile.user_info.order_state;
        profile['city'] = tempProfile.user_info.order_city;
        profile['address'] = tempProfile.user_info.order_address;
        profile['zip'] = tempProfile.user_info.order_zip;
      }
    }

    return profile;
  }

  profileEdit() 
  {
    this.userDataObj = this.tempProfile.therapistProfile;
    this.profileUpdate.get('name').setValue(this.userDataObj.user_info.order_name);
    this.profileUpdate.get('email').setValue(this.userDataObj.email); // or .user_info.order_email
    this.profileUpdate.get('phone').setValue(this.userDataObj.user_info.order_phone);
    this.profileUpdate.get('country').setValue(this.userDataObj.user_info.order_country);
    this.profileUpdate.get('state').setValue(this.userDataObj.user_info.order_state);
    this.profileUpdate.get('city').setValue(this.userDataObj.user_info.order_city);
    this.profileUpdate.get('address').setValue(this.userDataObj.user_info.order_address);
    this.profileUpdate.get('zip').setValue(this.userDataObj.user_info.order_zip);
  }

  onSubmit() {
    this.commonprovider.Alert.confirm(this.label.confirmMesg).then(
      (res: any) => {
        let formObj = this.profileUpdate.value;
        formObj.uid = this.commonprovider.getUserLoginId();
        this.commonprovider.showLoader(this.label.pleasewait);
        this.commonprovider.post(this.appsettings.profileUpdate, formObj).subscribe(res => {
          if (res.data.success) {
            this.profile = formObj;
            delete this.profile.uid;

            this.isEditFlag = false;
            this.commonprovider.hideLoader();
            this.commonprovider.showToast(res.data.message);

            // set user profile updated object throught out application
            let url = this.appsettings.userProfile
              + '?key=' + this.userDataObj.auth
              + '&id=' + this.userDataObj.id;

            this.commonprovider
              .get(url)
              .subscribe((resp: any) => {
                if (!resp.err_code) {
                  this.commonprovider.hideLoader();
                  this.setUserData(resp);
                } else {
                  this.commonprovider.showToast(resp.err_msg);
                }
                this.commonprovider.hideLoader();
              });

          }
          else {
            this.commonprovider.hideLoader();
            this.commonprovider.showToast(res.err_msg || this.label.profileupdateerr);
          }
        }, (err) => {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err);
        });
      },
      err => {
        this.commonprovider.showToast(err);
      }
    );
  }

  setUserData(resp: any) {
    this.userDataObj = Object.assign(this.userDataObj, resp.data);
    this.commonprovider.setUserData(this.userDataObj);
  }

  getCountryList() {
    let url = this.appsettings.getCountryList;

    this.commonprovider.showLoader(this.appsettings.countryLoader);
    this.commonprovider
      .get(url).subscribe((response: any) => {
        if (!response.err_code && response.data.count != 0) {
          this.countries = response.data.countries;
          this.commonprovider.hideLoader();
        } else {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(
            response.err_msg || this.label.error,
            5000
          );
        }
      },
        err => {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err.message);
        });
  }

  showCountry(ev: any) {
    /*let cntryId: any;
    this.selectedCountry.name ? (cntryId = this.selectedCountry.name) : (cntryId = null);
    const cntryModal = this.modal.create(CountryModulePage, { country: this.countries, id: cntryId });
    cntryModal.present();
    cntryModal.onDidDismiss(data => {
      this.selectedCountry = data;
      this.profileUpdate.controls['country'].setValue(this.selectedCountry.name);

    });*/
  }

  dismiss()
  {
    this.modal.dismiss();
  }

  goToBackUrl()
  {
    if(this.passedObj != undefined)
      this.location.back();
     else
      this.dismiss(); 
  }
}
