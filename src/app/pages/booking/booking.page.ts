import { Component, OnInit } from '@angular/core';
import { CommonProvider } from 'src/app/services/common/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AppointmentComponent } from './appointment/appointment.component';
import { UserbookingComponent } from './userbooking/userbooking.component';
import { Location } from '@angular/common';


@Component({
  selector: 'app-booking',
  templateUrl: './booking.page.html',
  styleUrls: ['./booking.page.scss'],
})
export class BookingPage implements OnInit {
  tab1root: any;
  tab2root: any;
  // tab3root: any;

  title: string;
  label: any;
  selectedTab: any;

  passedObj:any;

  constructor(public commonprovider: CommonProvider,    public route:ActivatedRoute,
    private router: Router,
    private modal:ModalController,
    public location:Location
    ) 
    { 
      this.label = this.commonprovider.getLabels().tabThree;


      /*this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.passedObj = this.router.getCurrentNavigation().extras.state.userdata;
        }
      });
      */
  

      //let userRole = this.passedObj.userRole;
      
      /*this.title = userRole ? this.label.title1 : this.label.title2;
  
      this.tab1root = this.getPage(userRole);
      this.tab2root = this.getPage(userRole);
  */
    }

  ngOnInit() 
  {
    let st = this.commonprovider.getUserLoginId();
    console.log("asdasdasd="+st);
    
    if (this.route.snapshot.data['special']) 
    {
      this.passedObj = this.route.snapshot.data['special'].state.userdata;
    }

      this.tab1root = this.getPage(this.passedObj.userRole);
      this.tab2root = this.getPage(this.passedObj.userRole);

      console.log("sdsadasd");
  }

  getPage(userRole: any) {
    return userRole
      ? AppointmentComponent
      : UserbookingComponent;
  }

  goToBackUrl()
  {
    this.location.back();
  }

}
