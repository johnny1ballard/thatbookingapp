import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CommonProvider } from 'src/app/services/common/common';
import { AppSettings } from 'src/app/services/app-settings';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { BookimgWizardDataService } from 'src/app/services/bookimg-wizard-data.service';
import { ProfileComponent } from '../../profile/profile.component';
import { InAppBrowserOptions,InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { MatStepper } from '@angular/material';
import { Location } from '@angular/common';
import { LoginComponent } from '../../login/login.component';
import { AppinfoComponent } from '../../login/appinfo/appinfo.component';


@Component({
  selector: 'app-booking-wizard',
  templateUrl: './booking-wizard.component.html',
  styleUrls: ['./booking-wizard.component.scss'],
})
export class BookingWizardComponent implements OnInit {

  label: any;
  config: any;
  signUplabel:any;

  bookingForm : FormGroup ;

  appointmentDate: any;

  selectedTherapist: any = {
    id: ''
  };

  selectedService: any = {
    service: '',
    id: ''
  };

  selectedCountry: any = {
    name: ''
  };

  usrType;
  guesEmailInp;
  guestUsrInp ;

  paymentType="Stripe";
  appPayment="Both";
  paymentTypeMsg = "Pay by card" ;

  couponCode;
  appliedCode="";
  validCoupon:boolean=true;
  applCpn:boolean=false;

  loggedIndUid;

  selectedTime:any=[];

  options: InAppBrowserOptions = {
    location: 'no',
    clearcache: 'yes',
    zoom: 'no',
    hardwareback: 'yes',
    closebuttoncaption: 'Close', //iOS only
    //toolbar: 'yes',
    //toolbarcolor: '#488aff',
    //hidenavigationbuttons: 'yes',
    hidespinner: 'no',
    //toolbarposition: 'top',
    //allowInlineMediaPlayback: 'no',
    //closebuttoncaption: '< Back',
    closebuttoncolor: '#FFFFFF'
  };

  @ViewChild('stepper',{static: false}) stepper: MatStepper;

  serviceFormGroup: FormGroup;

  public showServiceBtn:boolean = false;
  public showTherepistBtn:boolean = false;
  public showCalenderBtn:boolean = true;

  public finalCost= 0 ;

  constructor(public fb:FormBuilder, public commonprovider: CommonProvider,
    public appsettings: AppSettings,
    public route:ActivatedRoute,
    private router: Router,
    private modal:ModalController,
    public datepipe: DatePipe,
    public bookingwzrd:BookimgWizardDataService,
    public iab: InAppBrowser,
    public location:Location
    ) { }

  ngOnInit() 
  {
    this.showTherepistBtn =false;
    this.bookingForm = this.fb.group({
      serviceName: new FormControl('', Validators.required),
      sid: new FormControl('', Validators.required),
      did: new FormControl('', Validators.required),
      booking_date: ['', Validators.required],
      booking_start_time: new FormControl('', Validators.required),
      booking_end_time: new FormControl('', Validators.required),
      booking_total: new FormControl('0.00', Validators.required),
      booking_amount: new FormControl('0.00', Validators.required),
      booking_notw: [''],
      cnaame: ['', Validators.required],
      cemail: ['', Validators.required],
      cphone_code: ['91', Validators.required], // Hard
      cphone_no: ['', Validators.required],
      caddress: ['', Validators.required],
      ccity: ['', Validators.required],
      czip: ['', Validators.required],
      appliedCoupon: ['', Validators.required],
      aid: new FormControl('', Validators.required),
      terms: [true, Validators.required],

    });

    this.loggedIndUid = this.commonprovider.getUserLoginId();
    this.label = this.commonprovider.getLabels().booking;
    this.config = this.commonprovider.getConfig();
    this.signUplabel = this.commonprovider.getLabels().signup;

    this.selectedService = this.fb.group({
      id: new FormControl('', Validators.required)
    });

    this.selectedTherapist = this.fb.group({
      id: new FormControl('', Validators.required)
    });

    this.appointmentDate = new FormControl('', Validators.required) ;
    
    if(this.loggedIndUid == 0)
    {
      this.usrType = "guestuser" ; 
    
    }
    
    
    this.appPayment = this.config.payment_method ;
  
    if(this.appPayment == "Stripe")
    {
      this.paymentType = "Stripe" ;
      this.paymentTypeMsg = "Payment by card" ;
    }

    if(this.appPayment == "Offline")
    {
      this.paymentType = "Offline"
      this.paymentTypeMsg = "Please click the BOOK button to pay in store" ;
    }
    //subscriber
    this.eventSubscribe();


    /*this.serviceFormGroup = this.fb.group({
      selectedService: ['', Validators.required]
    });
    */
  }

  ionViewWillLoad() {
    this.config = this.commonprovider.getConfig();
    this.appPayment = this.config.payment_method ;

    if(this.appPayment == "Stripe")
    {
      this.paymentType = "Stripe" ;
      this.paymentTypeMsg = "Payment by card" ;
    }

    if(this.appPayment == "Offline")
    {
      this.paymentType = "Offline"
      this.paymentTypeMsg = "Please click the BOOK button to pay in store" ;
    }
    
  }

  setServiceData(data: any) {
    this.selectedService = data.selectedService;
    this.bookingForm.controls['serviceName'].setValue(this.selectedService.service_name);
    this.bookingForm.controls['sid'].setValue(this.selectedService.id);
    this.bookingForm.controls['booking_total'].setValue(this.selectedService.service_price);
    this.bookingForm.controls['booking_amount'].setValue(this.selectedService.service_price);

    //set default value of final cost
    this.finalCost = this.selectedService.service_price ;

  }

  setTherapistData(data: any) {
    this.selectedTherapist = data;
    this.bookingForm.controls['did'].setValue(data.id);
    this.bookingForm.controls['cnaame'].setValue(data.employee_name);
    this.bookingForm.controls['cemail'].setValue(data.employee_email);
    

  }

  setDate(dte: any) {
    this.appointmentDate = new Date(dte);
    let dt = this.datepipe.transform(this.appointmentDate, 'yyyy-MM-dd');
    this.bookingForm.controls['booking_date'].setValue(dt);
    
    /**
     * @veramatic : commented by veramatic as the beheviour has chane to show calendar and timeslot in one modal itself
     * set time here
     * this.showTimeSlots();
     */
    
  }

  setTime(bookTime)
  {
    if (bookTime) 
        {
          let endTime = this.selectedService.service_length; // Need to remove 00:
          this.bookingForm.controls['booking_start_time'].setValue(bookTime);
          this.bookingForm.controls['booking_end_time'].setValue(this.addTimes(bookTime, endTime));
        }
  }

    // Convert a time in hh:mm format to minutes
    timeToMins(time) {
      var b = time.split(':');
      return b[0] * 60 + +b[1];
    }
  
    // Convert minutes to a time in format hh:mm
    // Returned value is in range 00  to 24 hrs
    timeFromMins(mins) {
      function z(n) { return (n < 10 ? '0' : '') + n; }
      var h = (mins / 60 | 0) % 24;
      var m = mins % 60;
      return z(h) + ':' + z(m);
    }
  
    // Add two times in hh:mm format
    addTimes(t0, t1) {
      return this.timeFromMins(this.timeToMins(t0) + this.timeToMins(t1));
    }

    
    eventSubscribe() 
    {
      this.bookingwzrd.timeSlotData.subscribe((dateFilter) => {
        if (dateFilter) 
        {
          this.setDate(dateFilter.selectedDate);
          this.setTime(dateFilter.selectedTime) ;

          setTimeout(() => {           // set timeout is for state propogation
            this.stepper.next();
           }, 2);  
        
        }
      });

      this.bookingwzrd.serviceData.subscribe((obj)=>{
        if(obj.data == undefined)
          this.setServiceData(obj) ;
        else
          this.setServiceData(obj.data) ;

          setTimeout(() => {           // set timeout is for state propogation
            this.stepper.next();
           }, 2);  
        
      });


      this.bookingwzrd.therapistData.subscribe((obj)=>{
        this.setTherapistData(obj.selectedTherapist) ;

        setTimeout(() => {           // set timeout is for state propogation
          this.stepper.next();
         }, 2);  
      
      });

      //button related subscription

      this.bookingwzrd.serviceFlag.subscribe((obj)=>
        {
          if(obj)
          {
            this.showServiceBtn = true;
          }
               
        });

        this.bookingwzrd.TherapistFlag.subscribe((obj)=>
        {
          if(obj)
          {
            
            setTimeout(() => {           // set timeout is for state propogation
              this.showTherepistBtn = true;
             }, 2);  
          }
          else
          this.showTherepistBtn = false;
               
        });

    }

    async showProfile(event: any, therapistObj: any) {
      event.stopPropagation();
  
      //this.navCtrl.push(ProfilePage, { therapistProfile: therapistObj });
  
      let navigationExtras: NavigationExtras = {
        state: {
          userdata:{therapistProfile: therapistObj}
        }
      };
  
     // this.router.navigate(["pages",'profile'],navigationExtras);
     const mfl = await this.modal.create({component:ProfileComponent,componentProps:{"type":navigationExtras}});

        mfl.present();

  
  
    }


    onSubmit() 
    {
      let formObj = this.bookingForm.value;
      formObj.uid = this.commonprovider.getUserLoginId();
      delete formObj.serviceName;
  
      if(this.usrType == "guestuser")
      {
        //if ((this.guestUsrInp == undefined || this.guestUsrInp == "") && (this.guesEmailInp == undefined || this.guesEmailInp == "" )) {
        if (this.guestUsrInp == undefined  || this.guesEmailInp == undefined ) 
        {
            this.commonprovider.showToast("You need to fill guest user name and Email");
            return false;
        }

        if(this.bookingForm.controls['terms'].value == false)
        {
          this.commonprovider.showToast("Please accept terms and conditions to continue");
            return false;
        }

        formObj.uid =0 ;
    
        formObj.cnaame=this.guestUsrInp;
        formObj.cemail=this.guesEmailInp;
    
        formObj.package_name = this.config.package_name
        //formObj.fcmToken = this.commonprovider.getFCMToken();
        formObj.fcmToken = this.commonprovider.global.fcmToken;

      }
      

      /*
          if (!this.bookingForm.controls['serviceName'].value) {
            this.commonprovider.showToast(this.label.msgServiceName);
            return false;
          }
          if (!this.bookingForm.controls['booking_date'].value) {
            this.commonprovider.showToast(this.label.msgBookingDate);
            return false;
          }
          if (!this.bookingForm.controls['booking_start_time'].value) {
            this.commonprovider.showToast(this.label.msgSelectTime);
            return false;
          }
          if (!this.bookingForm.controls['booking_total'].value) {
            this.commonprovider.showToast(this.label.msgBookingTotal);
            return false;
          }
          if (!this.bookingForm.controls['cnaame'].value) {
            this.commonprovider.showToast(this.label.msgCname);
            return false;
          }
          if (!this.bookingForm.controls['cemail'].value) {
            this.commonprovider.showToast(this.label.msgCemail);
            return false;
          }
          if (!this.bookingForm.controls['cphone_no'].value) {
            this.commonprovider.showToast(this.label.msgCphoneNo);
            return false;
          }
          if (!this.bookingForm.controls['caddress'].value) {
            this.commonprovider.showToast(this.label.msgCaddress);
            return false;
          }
  
          if (!this.bookingForm.controls['ccity'].value) {
            this.commonprovider.showToast(this.label.msgCcity);
            return false;
          }
          if (!this.bookingForm.controls['czip'].value) {
            this.commonprovider.showToast(this.label.msgCzip);
            return false;
          }
       */
  
      formObj.paymentType = this.paymentType ; 
      this.commonprovider.showLoader(this.appsettings.bookLoader);
      this.commonprovider.post(this.appsettings.postBooking, formObj).subscribe(
        (response: any) => {
          this.commonprovider.hideLoader();
          if (!response.err_code) {
            if (!response.data.success) {
              this.commonprovider.showToast(response.data.err_msg, 5000);
            } else {
              // this.commonprovider.showToast(this.label.msgSuccess, 5000);

              if(this.paymentType == "Stripe" && response.data.order_detail.order_final_cost !=  "0.00")
              {
                  this.openInAppBrowser(response);
              }
              else
              {
                this.paymentDone(true);
              }
            }
          } else {
            this.commonprovider.showToast(response.err_msg, 5000);
            this.commonprovider.hideLoader();
          }
        },
        err => {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err.message || this.label.msgError);
        }
      );
    }
  
    openInAppBrowser(response) {
      let paymentStatus: any;
  
      const browser =
        this.iab
          .create(
            response.data.payment_url,
            "_blank",
            this.options // no spaces allowed in options!
          );
  
      browser.on('loadstop').subscribe((e) => {
        console.log("Loadstop: the url is"+e.url);
        paymentStatus = this.getQueryString('app_call', e.url);
        if (paymentStatus != null) {
          setTimeout(() => {
            browser.close();
          }, 2000)
        }
      });
  
      browser.on('exit').subscribe((e) => {
        this.paymentDone(paymentStatus);
      });
    }
  
    getQueryString(field: any, url: any) {
      let href = url;
      let reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
      let string = reg.exec(href);
      return string ? string[1] : null;
    };
  
    paymentDone(status: any) {
      if (status != null && status != 0) 
      {
        status ? this.commonprovider.showToast(this.label.msgPaymentSuccess) : this.commonprovider.showToast(this.label.msgPaymentError);
        this.showPaymentStatus(status);
      } else {
        this.commonprovider.showToast(this.label.msgPaymentError);
      }
    }
  
    showPaymentStatus(status) {
      let formObj = this.bookingForm.value;
      let obj = {
        status: status,
        data: {
          appointment: formObj.cnaame,
          //appointment: "Test User",
          service_name: formObj.serviceName,
          booking_date: formObj.booking_date,
          time: formObj.booking_start_time + ' - ' + formObj.booking_end_time
        }
      };
  
      if (status) {
        //obj.data['fees'] = this.config.currency_symbol + formObj.booking_amount;
        obj.data['fees'] = this.config.currency_symbol + this.finalCost;
        obj.data['note'] = formObj.booking_notw;
        obj.data['email'] = formObj.cemail;
      }
  
      if(this.paymentType == "Offline")
      {
          obj.data["Offline"] = true;
      }
      //this.navCtrl.push(SuccessPage, { 'paymentResponse': obj });
  
      let navigationExtras: NavigationExtras = {
        state: {
          paymentResponse: obj
        }
      };
  
      this.router.navigate(["pages",'success'],navigationExtras);
  
  
    }
  
    gotoPrevScreen()
    {
      this.stepper.previous();
    }

    test()
    {
      this.showPaymentStatus(false);
    }

    isServiceSelected()
    {
      if(this.selectedService.id == undefined || this.selectedService.id == "")
      {
        this.commonprovider.showToast("Select Service first.")
      }
        
     //   this.stepper.next();
    }

    isTherepistSelected()
    {
      if(this.selectedTherapist.id == undefined || this.selectedTherapist.id == "")
      {
        this.commonprovider.showToast("Select therapist first.")
      }
        
     //   this.stepper.next();
    }

    isTimeSelected()
    {
      if(this.bookingForm.controls['booking_start_time'].value == "" || this.bookingForm.controls['booking_start_time'].value == undefined )
      {
        this.commonprovider.showToast("Select day and time from the calendar.") ;
      }
        
     //   this.stepper.next();
    }

  goToBackUrl()
  {
    this.location.back();
  }

  async chngeUsrType($evnt)
  {
      //console.log("Event is "+$evnt);

      if($evnt.detail.value == "registeruser")
      {
        //call login component in modal 

        
       // this.router.navigate(["pages",'profile'],navigationExtras);
       const lgnMdl = await this.modal.create({component:LoginComponent,componentProps:{"lgnFlow":"booking"}});
  
       lgnMdl.present();

       lgnMdl.onDidDismiss().then(data => {
            console.log('onDidDismiss', data);
            if(data.data == undefined)
            {
               this.usrType = "guestuser" ; 
            }
            else if(data.data == "login successfull")
            {
              this.loggedIndUid = this.commonprovider.getUserLoginId(); 
              this.commonprovider.guestToLoggedin.emit({"loggedIndUid":this.loggedIndUid});
            }
            else
            {
                console.log("do noothing");
            }

          });  
  
    
      }
  }

   validateCoupon(event: any) {
    event.stopPropagation();

    //check if coupon is valid
    let formObj = [];

    formObj["couponCode"] = this.couponCode ;
    formObj["type"] = "validateCoupon" ;
    formObj["aid"] = this.config.id ;
    formObj["uid"] = this.commonprovider.getUserLoginId() ;

    this.commonprovider.showLoader(this.appsettings.couponLoader);
      this.commonprovider.post(this.appsettings.validateCoupon, formObj).subscribe(
        (response: any) => {
          this.commonprovider.hideLoader();
          if (!response.err_code) {
            if (!response.data.success) {
              this.finalCost = this.bookingForm.controls['booking_total'].value;
              this.validCoupon = false;
              this.applCpn=false ;
              this.appliedCode = "" ;
              this.bookingForm.controls['appliedCoupon'].setValue("");
              this.commonprovider.showToast(response.data.err_msg, 5000);
            } 
            else 
            {
              // this.commonprovider.showToast(this.label.msgSuccess, 5000);

              //code if validation is success
              let coupon = response.data.coupons;
              this.validCoupon = true;
              this.applCpn = true ;
              this.appliedCode = coupon["coupon_code"];
              //coupon discount_type =0 => percentage 
              //coupon discount_type =1 => fixed 
              let currentCost = this.bookingForm.controls['booking_total'].value ;
              let calculatedDiscount = 0;

              if(coupon["discount_type"] == 0)
              {
                  calculatedDiscount = (currentCost* coupon["discount"])/100;
              }
          
              if(coupon["discount_type"] == 1)
              {
                  //we cehck if the coupon discount is greater than service value if not thow an error 

                  calculatedDiscount = coupon["discount"];
                  if(Number(currentCost) < Number(calculatedDiscount))
                  {
                     //payment error 
                     this.finalCost = this.bookingForm.controls['booking_total'].value;
                      this.validCoupon = false;
                      this.applCpn=false ;
                      this.appliedCode = "" ;
                      this.bookingForm.controls['appliedCoupon'].setValue("");
                     this.commonprovider.showToast("Coupon value should not be greater than service value" || this.label.msgError);

                     return ;
                  }
              }
              
              let finalCst = currentCost - calculatedDiscount ;

              this.finalCost = finalCst;

              this.bookingForm.controls['appliedCoupon'].setValue(coupon["coupon_code"]);
             
    
              this.bookingForm.controls["aid"].setValue(this.config.id);
              
            }
          } else {
            this.commonprovider.showToast(response.err_msg, 5000);
            this.commonprovider.hideLoader();
          }
        },
        err => {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err.message || this.label.msgError);
        }
      );

  }

  removeCode($event)
  {
              this.finalCost = this.bookingForm.controls['booking_total'].value;
              this.applCpn = false;
              this.appliedCode = "" ;
              this.couponCode="";
              this.bookingForm.controls['appliedCoupon'].setValue("");

  }

  chngePaymentType($evnt)
  {
      //console.log("Event is "+$evnt);

      if($evnt.detail.value == "Stripe")
      {
        this.paymentType = "Stripe" ;
        this.paymentTypeMsg = "Payment by card" ;
      }

      if($evnt.detail.value == "Offline")
      {
        this.paymentType = "Offline" ;
        this.paymentTypeMsg = "Please click the BOOK button to pay in store" ;
      }
  }

  async showTerms() 
  {
    
    // this.navCtrl.push(InfoPage, { 'type': 'terms' });

   const modal = await this.modal.create({
      component:AppinfoComponent,
      componentProps:{'type':'terms'}
    });

    return await modal.present();

  }

}
