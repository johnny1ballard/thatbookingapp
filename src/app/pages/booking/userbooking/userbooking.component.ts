import { Component, OnInit } from '@angular/core';
import { AppSettings } from 'src/app/services/app-settings';
import { CommonProvider } from 'src/app/services/common/common';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-userbooking',
  templateUrl: './userbooking.component.html',
  styleUrls: ['./userbooking.component.scss'],
})
export class UserbookingComponent implements OnInit {

  bookingList: any=[];
  label: any;
  config: any;
  doctorIcon: any;
  filterType: string;
  limit: number = 10;
  limitstart: number = 0;
  totalCount: number = 0;
  isLoadMore: boolean = true;
  

  constructor(public appsettings: AppSettings,
    public commonprovider: CommonProvider,params: NavParams) 
  { 

    //this.params = params;
      //console.log("asdas="+params); // returns NavParams {data: Object}
    this.filterType = params.data.id ;
    this.bookingList = [];
    //this.getBookingList(this.limit, this.limitstart);
    this.label = this.commonprovider.getLabels().mybooking;
    this.config = this.commonprovider.getConfig();
    this.doctorIcon = this.commonprovider.getSvgIcos().doctor.data;

  }

  ngOnInit() 
  {
    let st = this.commonprovider.getUserLoginId();
    console.log("asdasdasd="+st);
    //this.filterType = this.navCtrl['tabId'];//commneted by prashant
    this.bookingList = [];
    this.getBookingList(this.limit, this.limitstart);
    this.label = this.commonprovider.getLabels().mybooking;
    this.config = this.commonprovider.getConfig();
    this.doctorIcon = this.commonprovider.getSvgIcos().doctor.data;
  }

  getBookingList(limit: number, limitstart: number) {
    let url = this.appsettings.getBookingList
      + '?uid=' + this.commonprovider.getUserLoginId()
      + '&limit=' + limit
      + '&limitstart=' + limitstart
      + '&filterType=' + this.filterType;

    this.commonprovider.showLoader(this.appsettings.bookingLoader);
    this.commonprovider
      .get(url)
      .subscribe(
        (response: any) => {
          if (!response.err_code && response.data.count != 0) {
            if(response.data.orders != undefined)
              this.bookingList = this.bookingList.concat(response.data.orders);

            this.totalCount = this.limitstart + this.limit;
            if (this.bookingList.length == response.data.count) {
              this.isLoadMore = false;
            }

            this.commonprovider.hideLoader();
          } else {
            this.commonprovider.hideLoader();
            //this.bookingList = this.getEmptyDataList();
            this.commonprovider.showToast(
              response.err_msg || this.label.errMsg,
              5000
            );
          }
        },
        err => {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err.message);
        }
      );
  }

  getEmptyDataList() {
    return [
      {
        empty: true,
        booking_date: "1111-01-01",
        employee_detail: {
          employee_name: 'NO RECORD FOUND',
          employee_phone: 'xxxxxxxxxx',
          employee_email: 'xxxxx'
        },
        order_final_cost: '00.00',
        order_status: 'C',
        order_name: 'BOOK NEW APPOINTMENT',
        start_time: 'xxxxxxxx',
        nslots: 'xx',
      }
    ]
  }

  doInfinite(ev: any) {
    setTimeout(() => {
      this.limitstart = this.totalCount;
      this.getBookingList(this.limit, this.limitstart);
      ev.complete();
    }, 1000);
  }

}
