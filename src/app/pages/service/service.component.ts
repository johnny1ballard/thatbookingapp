import { Component, OnInit, NgModuleFactory, Input, Output, EventEmitter } from '@angular/core';
import { AppSettings } from 'src/app/services/app-settings';
import { CommonProvider } from 'src/app/services/common/common';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { BookimgWizardDataService } from 'src/app/services/bookimg-wizard-data.service';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss'],
})
export class ServiceComponent implements OnInit {

  @Input() id: string;

  

  serviceFlow: boolean = false;
  userData: any = {};
  services: any = false;
  servicemsg: any;
  selectedService: any = {
    id: ''
  };
  dataObj: any;
  config: any;
  label: any;

  public passedObj:any;

  public buttonText = "" ;
  public buttonIcon = "" ;

  //wizard related variable
  @Input() frmWizard:boolean = false ;
  
  constructor(    
    public appsettings: AppSettings,
    public commonprovider: CommonProvider,
    public route:ActivatedRoute,
    private router: Router,
    private modal:ModalController,
    public bookingwzrd:BookimgWizardDataService
  ) 
  { 
    this.label = this.commonprovider.getLabels().servicesModal;

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation() !=null && this.router.getCurrentNavigation().extras.state) {
        this.passedObj = this.router.getCurrentNavigation().extras.state.userdata;
      }
    });

  }

  ngOnInit() 
  {
    let st = this.commonprovider.getUserLoginId();
    this.config = this.commonprovider.getConfig();
    this.dataInit();
    console.log("asdasdasd="+this.frmWizard);
  }

  ionViewWillLoad() {
    this.config = this.commonprovider.getConfig();
    this.dataInit();
  }

  dataInit() 
  {
    if(this.passedObj != undefined)
    {
      this.serviceFlow = this.passedObj.serviceFlow;

      this.passedObj.id ? (this.selectedService.id = this.passedObj.id): 'nothing';

    }
    
    this.commonprovider.showLoader(this.appsettings.serviceLoader);
    this.commonprovider
      .get(this.appsettings.getServices + '?uid=' + this.config.therapist)
      .subscribe(
        (response: any) => {
          if (!response.err_code && response.count != 0) {
            this.services = response.data.services;
            this.commonprovider.hideLoader();

            //emit ann event to specify that services has come
            this.bookingwzrd.serviceFlag.emit(true);
          } else {
            this.commonprovider.hideLoader();
            this.commonprovider.showToast(
              response.err_msg || this.label.error,
              5000
            );
          }
          response.count
            ? ''
            : (this.servicemsg = this.label.serviceMsg);
        },
        err => {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err.message);
        }
      );
  }

  closeModal() {
    this.modal.dismiss();
  }

  selectService(event: any, obj: any) {
    event.stopPropagation();
    this.selectedService = obj;
    this.dataObj = obj;

    let TIME_IN_MS = 250;
    setTimeout(() => {
      this.setService();
    }, TIME_IN_MS);
  }

  setService() {
    this.bookingwzrd.TherapistFlag.emit(false);
    if (this.serviceFlow) 
    {
      let obj = {
        serviceFlow: this.serviceFlow,
        userData: this.userData,
        therapistId: '',
        selectedService: this.selectedService
      };

      //this.navCtrl.push(TherapistPage, { data: obj });

      let navigationExtras: NavigationExtras = {
        state: {
          data: obj
        }
      };

      if(!this.frmWizard)
      {
        /** @veramatic 
         * since to protect backward compatibility to eneable modals nd componnet propogation ,this if is use 
         * the else will contain logic for form wizard
         */
        this.router.navigate(["pages","therapist"],navigationExtras);

      }
      else
      {
        //this logic is for the passing data by form wizard
        //use service to interreact
        //this.bookingwzrd.setServiceData({ data: obj });
        this.bookingwzrd.serviceData.emit({ data: obj });
      }
      
    } else {
      if(!this.frmWizard)
      {
        /** @veramatic 
         * since to protect backward compatibility to eneable modals nd componnet propogation ,this if is use 
         * the else will contain logic for form wizard
         */
        this.modal.dismiss(this.dataObj);
      }
      else
      {
        //this logic is for the passing data by form wizard
        //use service to interreact
        //this.bookingwzrd.setServiceData({ data: this.dataObj });
        this.bookingwzrd.serviceData.emit({ selectedService: this.dataObj });
      }
    }
  }

  /* viewDetail(event: any, str: any) {
    event.stopPropagation();
    var plainText = str.replace(/<[^>]*>/g, '');
    this.commonprovider.showToaster(plainText, '3500');
  } */

  viewDetail(event: any, item: any) {
    event.stopPropagation();

    let title = 'Appointment Details';
    let msg = '<div>'
      + '<b>Service name </b>: ' + item.service_name + '<br>'
      + '<b>Service Fees </b>: ' + this.config.currency_symbol + item.service_price + '<br>'
      + '<b>Service description </b>: ' + item.service_description + '<br>'
      + '</div>';

    this.commonprovider.showAlert(title, msg);
  }

}
