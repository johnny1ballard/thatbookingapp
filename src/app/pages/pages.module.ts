import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule, NavParams } from '@ionic/angular';

import { PagesPageRoutingModule } from './pages-routing.module';

import { PagesPage } from './pages.page';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CommonProvider } from '../services/common/common';
import { HttpClientModule } from '@angular/common/http';
import { Network } from '@ionic-native/network/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { AppSettings } from '../services/app-settings';
import { GlobalProvider } from '../services/global/global';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { AppinfoComponent } from './login/appinfo/appinfo.component';
import { SignupComponent } from './login/signup/signup.component';
import { ProfileComponent } from './profile/profile.component';
import { ContactComponent } from './contact/contact.component';
import { PipesModule } from '../pipes/pipes.module';
import { EmptyComponent } from './empty/empty.component';
import { ServiceComponent } from './service/service.component';
import { SuperTabsModule } from '@ionic-super-tabs/angular';
import { TherapistComponent } from './therapist/therapist.component';
import { BookAppointmentComponent } from './booking/book-appointment/book-appointment.component';
import { TimeslotsComponent } from './timeslots/timeslots.component';
import { DateComponent } from './date/date.component';
import { SuccessComponent } from './success/success.component';
import { CalendarModule } from 'ion2-calendar';
import { CalendarComponent } from './date/calendar/calendar.component';
import { BlockoutComponent } from './blockout/blockout.component';
import { BookingPageModule } from './booking/booking.module';
import {MatStepperModule} from "@angular/material";
import { BookingWizardComponent } from './booking/booking-wizard/booking-wizard.component';
import { TermComponent } from './login/term/term.component';
import { OffersComponent } from './offers/offers.component';
import { OffersModule } from './offers/offers.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PagesPageRoutingModule,
    HttpClientModule,
    //IonicStorageModule.forRoot(),
    PipesModule,   
    SuperTabsModule.forRoot(),
    CalendarModule,
    BookingPageModule,
    MatStepperModule,
    OffersModule
    
  ],
  declarations: [PagesPage,LoginComponent,DashboardComponent,AppinfoComponent,SignupComponent,ProfileComponent,
                ContactComponent,EmptyComponent,ServiceComponent,TherapistComponent,BookAppointmentComponent,
                TimeslotsComponent,DateComponent,SuccessComponent,CalendarComponent,BlockoutComponent,BookingWizardComponent,TermComponent],
  providers:[Network,
    Toast,
    AppSettings,
    DatePipe,
    //FCM,
    InAppBrowser,
  //  AppVersion,
  
  
  ],
  entryComponents:[AppinfoComponent,SignupComponent,CalendarComponent,ProfileComponent],
  exports:[EmptyComponent],

})
export class PagesPageModule {}
