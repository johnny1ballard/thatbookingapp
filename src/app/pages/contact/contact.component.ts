import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonProvider } from 'src/app/services/common/common';
import { AppSettings } from 'src/app/services/app-settings';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { TherapistComponent } from '../therapist/therapist.component';
import { ProfileComponent } from '../profile/profile.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  label: any;
  contactForm: FormGroup;
  userData: any;
  config: any;
  selectedTherapist: any = {
    id: '',
    employee_name: ''
  };
  employeeDr: any;

  public passedObj:any;

  componentName:string="selectTherapist";
  templateType:string="list";

  constructor(public formBuilder: FormBuilder,
    public commonprovider: CommonProvider,
    public appsettings: AppSettings,
    public modal: ModalController,
    //public view: ViewController,
    public route:ActivatedRoute,
    private router: Router,
    public location:Location
    )
    { 
      this.contactForm = this.formBuilder.group({
        message: ['', Validators.compose([
          Validators.required,
          Validators.pattern("^[A-Za-z0-9 _!?@#$&()\\-`.+,/\]*[A-Za-z0-9!?@#$&()\\-`.+,/\][A-Za-z0-9 _!?@#$&()\\-`.+,/\]*$")
        ])],
        subject: ['', Validators.compose([
          Validators.required,
          Validators.pattern("^[A-Za-z0-9 _!?@#$&()\\-`.+,/\]*[A-Za-z0-9!?@#$&()\\-`.+,/\][A-Za-z0-9 _!?@#$&()\\-`.+,/\]*$")
        ])]
      })
  
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.passedObj = this.router.getCurrentNavigation().extras.state.userdata;
        }
      });

      //this.userData = this.passedObj.contactData ;
    }


  ngOnInit() 
  {
    this.userData = this.passedObj.contactData;

    this.config = this.commonprovider.getConfig();
    this.dataInit();
  }

  ionViewWillLoad() {
    this.config = this.commonprovider.getConfig();
    this.dataInit();
  }

  dataInit() {
    this.label = this.commonprovider.getLabels().contact;
    
    this.commonprovider.showLoader("Please wait");
    this.commonprovider
      .get(this.appsettings.getAppEmployee + '?uid=' + this.config.therapist)
      .subscribe(
        (response: any) => {
          if (!response.err_code) {
            this.employeeDr = response.data.employees;
            this.commonprovider.hideLoader();
          } else {
            this.commonprovider.hideLoader();
            this.commonprovider.showToast(
              response.err_msg || 'Error 404',
              5000
            );
          }
        },
        err => {
          this.commonprovider.hideLoader();
          this.commonprovider.showToast(err.message);
        }
      );

  }

  async showTherapist(event: any) {
    event.stopPropagation();

    let obj: any = {
      data: {
        therapistId: this.selectedTherapist.id
      }, contactTherapist: this.employeeDr
    }

    const therapistModal = await this.modal.create({component:TherapistComponent,
                                                  componentProps:{'data':obj}}
                                                );
    
    therapistModal.onDidDismiss().then((data) => {
                                            if (data) {
                                              this.setTherapistData(data.data);
                                            }
                                        });

    await therapistModal.present();

  }

  setTherapistData(data) {
    this.selectedTherapist = data;
  }

  async showProfile(event: any, therapistObj: any) {
    event.stopPropagation();
    //this.navCtrl.push(ProfilePage, { therapistProfile: therapistObj });

   /* let navigationExtras: NavigationExtras = {
      state: {
        therapistProfile: therapistObj
      }
    };

    this.router.navigate(["pages",'profile'],navigationExtras);
*/

    let navigationExtras: NavigationExtras = {
      state: {
        userdata:{therapistProfile: therapistObj}
      }
    };
    const mfl = await this.modal.create({component:ProfileComponent,componentProps:{"type":navigationExtras}});

    mfl.present();



  }

  onSubmit() {
    if (!this.contactForm.value.subject) {
      this.commonprovider.showToast(this.label.msgSubject);
      return 0;
    }

    if (!this.contactForm.value.message) {
      this.commonprovider.showToast(this.label.msgMessage);
      return 0;
    }

    if (!this.selectedTherapist.id) {
      this.commonprovider.showToast(this.label.msgTherapist);
      return 0;
    }

    let formObj = this.contactForm.value;
    formObj.did = this.selectedTherapist.id;
    formObj.pid = this.commonprovider.getUserLoginId();

    this.commonprovider.Alert
      .confirm(this.label.msgConfirm)
      .then(
        (res: any) => {
          this.commonprovider.showLoader('Please wait');
          this.commonprovider
            .post(this.appsettings.contactTherapist, formObj)
            .subscribe(res => {
              if (res.data.success) {
                this.commonprovider.showToast(this.label.msgSuccess);
                this.selectedTherapist = {}
                this.contactForm.reset();
                this.commonprovider.hideLoader();
              }
              else {
                this.commonprovider.hideLoader();
                this.commonprovider.showToast(res.err_msg);
              }
            }, (err) => {
              this.commonprovider.hideLoader();
              this.commonprovider.showToast(err);
            });
        },
        err => {
          this.commonprovider.showToast(err);
        }
      );
  }

  goToBackUrl()
  {
    this.location.back();
  }
}
