import { Component, OnInit, Input } from '@angular/core';
import { ModalController, Events } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { CalendarModalOptions, CalendarModal, DayConfig } from 'ion2-calendar';
import { CalendarComponent } from './calendar/calendar.component';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss'],
})
export class DateComponent implements OnInit {


  dateRange: { from: Date; to: Date } = {
    from: new Date(),
    to: new Date()
  };

  @Input('config') config:any;
  selectedDate: string = '';


  constructor( public modal: ModalController,
    public datepipe: DatePipe,
    public events: Events) 
 { 
  this.selectedDate = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
 }

  ngOnInit() {}

  async openCalendar(config?: any) 
  {

    let _daysConfig: DayConfig[] = [];

  for (let i = 0; i < 31; i++) {
    _daysConfig.push({
      date: new Date(2017, 0, i + 1),
      subTitle: `$${i + 1}`
    })
  }


    let startDate = new Date();
    //let endDate = new Date(startDate.getFullYear(),startDate.getMonth()+2,0);
    let endDate = new Date();
    endDate.setMonth(endDate.getMonth() + 2);

    this.config = config ? config : this.config;

    if (this.config.type === 'icon') 
    {
      //startDate.setMonth(startDate.getMonth() - 3);
      endDate = new Date(startDate.getFullYear(),startDate.getMonth()+2,0);
    
      //endDate = new Date();
      //endDate.setMonth(endDate.getMonth() + 3);
    }

    /*const options: CalendarModalOptions = {
      autoDone: true,
      pickMode: this.config.pickMode,
      title: this.config.title,
      closeLabel: this.config.closeLabel,
      doneLabel: this.config.doneLabel,
      from: startDate,
      to: endDate,
      defaultScrollTo: new Date(),
      //defaultDateRange: this.dateRange,
      //disableWeeks: this.config.disableWeeks
 //     daysConfig: _daysConfig
 
    };

    let myCalendar = await  this.modal.create({
      component: CalendarModal,
      componentProps: { options }
    });
*/

const options: any = {
  autoDone: true,
  pickMode: this.config.pickMode,
  title: this.config.title,
  closeLabel: this.config.closeLabel,
  doneLabel: this.config.doneLabel,
  from: startDate,
  to: endDate,
  defaultScrollTo: new Date(),
  //defaultDateRange: this.dateRange,
  //disableWeeks: this.config.disableWeeks
//     daysConfig: _daysConfig
serviceData:this.config.serviceData,
therapistData:this.config.therapistData,

};


let myCalendar = await  this.modal.create({
  component: CalendarComponent,
  componentProps: { options }
});
    myCalendar.present();

    myCalendar.onDidDismiss().then((event) => {
      if (event.data != undefined) 
      {
        this.selectedDate = event.data.selectedDate;
        this.events.publish('date:selected', event.data);
      }
    });


  }

}
