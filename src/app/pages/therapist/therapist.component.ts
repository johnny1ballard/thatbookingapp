import { Component, OnInit, Input } from '@angular/core';
import { CommonProvider } from 'src/app/services/common/common';
import { AppSettings } from 'src/app/services/app-settings';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProfileComponent } from '../profile/profile.component';
import { BookimgWizardDataService } from 'src/app/services/bookimg-wizard-data.service';

@Component({
  selector: 'app-therapist',
  templateUrl: './therapist.component.html',
  styleUrls: ['./therapist.component.scss'],
})
export class TherapistComponent implements OnInit {

  @Input() data;

  label: any;
  config: any;
  therapists: any[] = [];
  selectedService: any = {
    id: ''
  };
  userData: any = {};
  selectedTherapist: any = {
    id: false
  };
  serviceFlow: boolean = false;
  therapistforcontact: any;

  public passedObj:any;

    //wizard related variable
    @Input() frmWizard:boolean = false ;

    @Input() frmWizrdService:any ;


  constructor(public modal: ModalController,
      public commonprovider: CommonProvider,
      public appsettings: AppSettings,
      public route:ActivatedRoute,
      private router: Router,
      public modal1: ModalController ,
      public bookingwzrd:BookimgWizardDataService
    ) 
    { 
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation() !=null && this.router.getCurrentNavigation().extras.state) {
          this.passedObj = this.router.getCurrentNavigation().extras.state;
        }
      });

    }

  ngOnInit() 
  {
    this.label = this.commonprovider.getLabels().therapist;
    this.config = this.commonprovider.getConfig();
    
    let obj ;
    let contactTherapist ;


    if(this.frmWizard)
    {
      this.bookingwzrd.serviceData.subscribe((selectedService)=>{
        if(selectedService.data != undefined)
        this.getTherapistByService(selectedService.data) ;
        else
          this.getTherapistByService(selectedService) ;
      });
    }

    if(this.passedObj != undefined)
    {
      obj = this.passedObj.data ;
      contactTherapist = this.passedObj.contactTherapist;

    }
    else
    {
      obj =  this.data ; 
      contactTherapist = this.data.contactTherapist

    }
      
    

    if (contactTherapist) {
      this.therapistforcontact = contactTherapist;
      this.selectedTherapist.id = obj.therapistId;
      this.getTherapistsForContact();
    } else {
      if(obj !=undefined)
      {
        this.serviceFlow = obj.serviceFlow;
        this.userData = obj.userData;
        this.selectedTherapist.id = obj.therapistId;
        this.selectedService = obj.selectedService;
        this.getTherapists();
      }
      
    }

  }

  getTherapists() {
    if (this.selectedService.id) {
      this.therapists = [];
      this.commonprovider.showLoader(this.label.getTherapists);
      this.commonprovider
        .get(
          this.appsettings.getTherapistsList
          + '?uid=' + this.config.therapist
          + '&sid=' + this.selectedService.id
        )
        .subscribe(
          (resp: any) => {
            if (!resp.err_code) {
              var data = resp.data;
              this.therapists = resp.data.count ? data.employees : this.getDemoData();
              this.commonprovider.hideLoader();

              this.bookingwzrd.TherapistFlag.emit(true);
            } else {
              this.commonprovider.hideLoader();
              this.commonprovider.showToast(resp.err_msg);
            }
          },
          error => {
            this.commonprovider.showToast(error.message);
            this.therapists = this.getDemoData();
            this.commonprovider.hideLoader();
          }
        );
    }
  }

  getTherapistsForContact() {
    this.therapists = this.therapistforcontact;
  }

  selectTherapist(dataObj: any) {
    let TIME_IN_MS = 250;
    this.selectedTherapist = dataObj;

    this.bookingwzrd.therapistData.emit({ selectedTherapist:dataObj });

    if(!this.frmWizard)
    {
      setTimeout(() => {
        this.modal.dismiss(this.selectedTherapist);
      }, TIME_IN_MS);

    }
    
  }

  bookNow(dataObj: any) {
    let TIME_IN_MS = 200;

    this.selectedTherapist = dataObj;
    let obj = {
      serviceFlow: !this.serviceFlow,
      data: {
        selectedTherapist: this.selectedTherapist,
        selectedService: this.selectedService
      }
    };

    if (!this.serviceFlow) {
      this.selectTherapist(dataObj);
    } else {

      if(!this.frmWizard)
        {
          setTimeout(() => {

            //this.navCtrl.push(BookingPage, obj);
            
            let navigationExtras: NavigationExtras = {
              state: {
                userdata: obj
              }
            };
    
            this.router.navigate(["pages","bookAppointment"],navigationExtras);
    
    
          }, TIME_IN_MS);    
        }
        else
        {
          this.selectTherapist(dataObj);
        }

      
    }
  }

  async showProfile(therapistObj: any) 
  {
    //this.navCtrl.push(ProfilePage, { therapistProfile: therapistObj });

    let navigationExtras: NavigationExtras = {
          state: {
            userdata:{therapistProfile: therapistObj}
          }
        };

       /* this.router.navigate(["pages","profile"],navigationExtras);
*/
        //open in modal

        const mfl = await this.modal1.create({component:ProfileComponent,componentProps:{"type":navigationExtras}});

        mfl.present();

  }

  closeModal() {
    this.modal.dismiss();
  }

  getDemoData() {
    return [];
  }

  getTherapistByService(obj)
  {
        this.serviceFlow = obj.serviceFlow;
        this.userData = obj.userData;
        this.selectedTherapist.id = obj.therapistId;
        this.selectedService = obj.selectedService;
        this.getTherapists();
  }

}
