import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagesPage } from './pages.page';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { ContactComponent } from './contact/contact.component';
import { ServiceComponent } from './service/service.component';
import { AppinfoComponent } from './login/appinfo/appinfo.component';
import { BookingPageModule } from './booking/booking.module';
import { BookingdataResolverService } from '../services/bookingdata-resolver.service';
import { TherapistComponent } from './therapist/therapist.component';
import { BookAppointmentComponent } from './booking/book-appointment/book-appointment.component';
import { BlockoutComponent } from './blockout/blockout.component';
import { BookingWizardComponent } from './booking/booking-wizard/booking-wizard.component';
import { AuthGuardService } from '../services/auth-guard.service';
import { SuccessComponent } from './success/success.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path:'pages',
    component:PagesPage,
    canActivate:[AuthGuardService],
    children:[
      { path:'',redirectTo:'dashboard',pathMatch:'full' },
      { path:'dashboard',component:DashboardComponent },
      { path:'profile',component:ProfileComponent},
      { path:'contact',component:ContactComponent },
      { path:'services',component:ServiceComponent },
      { path:'info',component:AppinfoComponent },
      {
        path: 'booking',
        loadChildren: () => import('./booking/booking.module').then( m => m.BookingPageModule),
        resolve:{special:BookingdataResolverService}
      },
      { path:'therapist',component:TherapistComponent },
      //{ path:'bookAppointment',component:BookAppointmentComponent},
      { path:'bookAppointment',component:BookingWizardComponent},
      { path:'blockout',component:BlockoutComponent},
      { path:'success',component:SuccessComponent},
      { path:'offers',
        loadChildren: () => import('./offers/offers.module').then( m => m.OffersModule),
        //resolve:{special:BookingdataResolverService}
      },
        
    ]
  },
  /*{
    path: 'booking',
    loadChildren: () => import('./booking/booking.module').then( m => m.BookingPageModule)
  }
  */
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesPageRoutingModule {}
