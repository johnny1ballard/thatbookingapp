import { Component, OnInit, Input } from '@angular/core';
import { CommonProvider } from 'src/app/services/common/common';

@Component({
  selector: 'app-empty',
  templateUrl: './empty.component.html',
  styleUrls: ['./empty.component.scss'],
})
export class EmptyComponent implements OnInit {
  @Input() templateType: string;
  @Input() componentName: string;
  componentActive: string;
  config: any;
  label: any;

  constructor( public commonprovider: CommonProvider,
    ) 
    { 
      this.label = this.commonprovider.getLabels().empty;

      this.config = {
        // Page
        booking: {
          icon: 'ios-calendar-outline',
          message: this.label.msgBooking
        },
        block: {
          icon: 'ios-calendar-outline',
          message: this.label.msgBlockOut
        },
        empty: {
          icon: 'ios-alert',
          message: this.label.msgError
        },
        // List
        therapist: {
          icon: 'ios-contact',
          message: this.label.msgTherapist,
          subTitle: this.label.subTitleTherapist
        },
        selectTherapist: {
          icon: 'ios-contact',
          message: this.label.msgSelectTherapist,
          subTitle: this.label.subTitleSelectTherapist
        },
        service: {
          icon: 'ios-construct',
          message: this.label.msgService,
          subTitle: this.label.subTitleService
        }
      };
  
    }

  ngOnInit() 
  {
    this.componentActive = this.componentName ? this.componentName : 'empty';

    console.log("dfsd="+this.componentActive);

  }

}
