import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { CommonProvider } from '../services/common/common';
import { AppSettings } from '../services/app-settings';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { Router } from '@angular/router';

import * as _ from 'lodash';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  errorFlag: boolean = false;
  
  constructor( private platform: Platform,
    private splashScreen: SplashScreen,
    public commonprovider: CommonProvider,
    public appSettings: AppSettings,
    private appVersion: AppVersion,
    public fcm: FCM,
    public router:Router
  ) 
  {

  }

  ngOnInit() 
  {
    this.getConfig();
  }

  getConfig() {
    let packageName = 'com.limedrive.tp408';

    this.commonprovider.showLoader('Initializing App ...');

    this.appVersion.getPackageName()
      .then((resPackageName) => {
        packageName = resPackageName;

        // Set in build.sh shell script file for dynamic URL
        // this.appSettings.set_API_ENDPOINT('https://' + packageName + '/api');

        let devicePlatform = this.platform.is('ios') ? 'ios' : 'android';
        this.commonprovider.setPackageName(packageName, devicePlatform);
        this.callGetConfig(packageName);
      })
      .catch((err) => {
        console.log("Error:", err);
        this.commonprovider.showToast('Error while retriving package!');

        /**
         *  since app version is not working on most of the phone 
         * adding the fix for the same  by explicitly specifying the package name 
         */  

        let devicePlatform = this.platform.is('ios') ? 'ios' : 'android';
        this.commonprovider.setPackageName(packageName, devicePlatform);
        
        this.callGetConfig(packageName); // For web
		
      });
  }

  callGetConfig(packageName: string) 
  {
    let URL = this.appSettings.getAppConfig + '?pkgname=' + packageName;

    this.commonprovider
      .get(URL)
      .subscribe(
        (response: any) => {
          this.errorFlag = false; 
          if (!response.err_code && response.data && response.data.config) {
            this.commonprovider.setConfig(response.data.config);
            this.setConfigSync(response.data.config);
            
            this.router.navigate(["pages","dashboard"]);

            this.commonprovider.hideLoader();
            //hide splash here :) 
            this.splashScreen.hide();

          } else {
            this.commonprovider.hideLoader();
            this.commonprovider.showToast(response.err_msg || 'Error in config', 5000);
          }
        },
        err => {
          this.errorFlag = true;
          this.commonprovider.hideLoader();
          this.commonprovider.showToast("Error in get config response "+err.message);
          
        }
      );
  }

  setConfigSync(config) {
    let label = this.commonprovider.getLabels();
    this.commonprovider.setLabels(_.merge(label, config.label));
  }

  sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }


}
