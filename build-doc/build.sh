# App Name
sed -i -e 's/<name>bookingMobileApp<\/name>/<name>Heather Wax<\/name>/' config.xml

# Package Name
sed -i -e 's/id="com.limedrive.tp408"/id="my.thatbooking.app"/' config.xml

# Base URL
sed -i '' "s/\'http:\/\/thatbooking.limedrive.co.uk\/thatbooking\/api\'; \/\/ Base URL/\'https:\/\/my.thatbooking.app\/api\'; \/\/ Base URL/" src/providers/app-settings.ts

# Android release version code eg. /--versionCode=<Old>/--versionCode=<new>/
sed -i -e 's/--versionCode=1/--versionCode=2/' build-doc/play-store/release.sh

# Root color
sed -i -e 's/#488aff; \/\/ primary/#FCAB31; \/\/ primary/' src/theme/variables.scss
sed -i -e 's/#FFFFFF; \/\/ nbg1/#FFFFFF; \/\/ nbg1/' src/theme/variables.scss

# Primary color
sed -i -e 's/#488aff, \/\/ primary/#FCAB31, \/\/ primary/' src/theme/variables.scss
sed -i -e 's/#000000, \/\/ nbgFont/#000000, \/\/ nbgFont/' src/theme/variables.scss
sed -i -e 's/#000000, \/\/ nfont/#000000, \/\/ nfont/' src/theme/variables.scss
sed -i -e 's/#696e79, \/\/ nfont1/#696e79, \/\/ nfont1/' src/theme/variables.scss
sed -i -e 's/#FFFFFF, \/\/ nbg1/#FFFFFF, \/\/ nbg1/' src/theme/variables.scss
sed -i -e 's/#FFFFFF, \/\/ nbg3/#FFFFFF, \/\/ nbg3/' src/theme/variables.scss
sed -i -e 's/#526cd0, \/\/ nlink/#3BA591, \/\/ nlink/' src/theme/variables.scss

# Secondary color
sed -i -e 's/#32db64, \/\/ secondary/#32db64, \/\/ secondary/' src/theme/variables.scss
sed -i -e 's/#f53d3d, \/\/ danger/#f53d3d, \/\/ danger/' src/theme/variables.scss
sed -i -e 's/#f4f4f4, \/\/ light/#f4f4f4, \/\/ light/' src/theme/variables.scss
sed -i -e 's/#222222, \/\/ dark/#222222, \/\/ dark/' src/theme/variables.scss

# Status color
sed -i -e 's/#48d1cc, \/\/ active-status/#48d1cc, \/\/ active-status/' src/theme/variables.scss
sed -i -e 's/#efa037, \/\/ inactive-status/#efa037, \/\/ inactive-status/' src/theme/variables.scss
sed -i -e 's/#ff7f50, \/\/ warning-msg/#ff7f50, \/\/ warning-msg /' src/theme/variables.scss
