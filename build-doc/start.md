
Namrtas-Mac-mini:~ namrta$ node -v
v10.15.3
Namrtas-Mac-mini:~ namrta$ npm -v
6.9.0
Namrtas-Mac-mini:~ namrta$ ionic -v

   _             _
  (_) ___  _ __ (_) ___
  | |/ _ \| '_ \| |/ __|
  | | (_) | | | | | (__
  |_|\___/|_| |_|_|\___|  CLI 4.11.0

Namrtas-Mac-mini:~ namrta$ ng --version

     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/


Angular CLI: 7.3.5
Node: 10.15.3
OS: darwin x64
Angular:
...

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.13.5
@angular-devkit/core         7.3.5
@angular-devkit/schematics   7.3.5
@schematics/angular          7.3.5
@schematics/update           0.13.5
rxjs                         6.3.3
typescript                   3.2.4

ionic start thatbookingapp blank --type=ionic-angular --package-id='com.weppsol.bookingmobileapp' --cordova --no-deps


# GIT

git clone https://gitlab.com/wep_sachin/thatbookingapp.git

cd existing_folder
git init
git remote add origin https://gitlab.com/wep_sachin/thatbookingapp.git
git add .
git commit -m "Initial commit"
git push -u origin master


# Chrome
open -n -a /Applications/Google\ Chrome.app --args --user-data-dir="/tmp/someFolderName" --disable-web-security;
