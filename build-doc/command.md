# Create new page
ionic generate page booking --no-module

# Create custom pipe
ionic g pipe objkeys

# Remove ionic webview - CORS issue
ionic cordova plugin rm cordova-plugin-ionic-webview

# Update typescript with range
npm install typescript@">=3.1.1 <3.3.0" --save

# Provider generate
ionic g provider global

# Provider generate
ionic cordova platform add ios

# iOS
ios-sim showdevicetypes
Xcode 9:
killall Simulator
xcrun simctl boot <device_id>
open `xcode-select -p`/Applications/Simulator.app
Xcode 8:
killall Simulator
xcrun simctl shutdown booted
xcrun instruments -w <device_id>
