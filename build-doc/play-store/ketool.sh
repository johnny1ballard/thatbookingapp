echo "INFO: Enter keystore password:weppsol"
echo "---------------------------------"

# Generate key
keytool -genkey -v -keystore ./resources/release-key.keystore -storetype PKCS12 -keyalg RSA -keysize 2048 -validity 10000 -alias booking
# Enter key password for <barter> : weppsol

# echo "Password:weppsol"
# The JKS keystore uses a proprietary format. It is recommended to migrate to PKCS12 which is an industry standard format using
# keytool -importkeystore -srckeystore ./resources/release-key.keystore -destkeystore ./resources/relase-key.keystore -deststoretype pkcs12
