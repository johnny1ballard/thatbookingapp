# Inside thatbookingapp
cd thatbookingapp

# Ionic android build release
# ionic cordova build android --prod --aot --minifyjs --minifycss --optimizejs --release -- -- --versionCode=1 -- -- --keystore=../resources/release-key.keystore --alias=booking

# Inside android
cd platforms/android

# Version increase
count=1
lineNumber=`awk '/cdvVersionCode=/{ print NR; }' gradle.properties`
oldCdvVersionCode=`awk '{if(NR=='$lineNumber') print $0}' gradle.properties`
oldCdvVersionCodeValue=$(echo $oldCdvVersionCode| sed 's/^cdvVersionCode=//')
newCdvVersionCodeValue=`expr $oldCdvVersionCodeValue + $count`
oldCodeString='cdvVersionCode='$oldCdvVersionCodeValue
newCodeString='cdvVersionCode='$newCdvVersionCodeValue

# Replace the old code cdvVersionCode
sed -i '' 's/'$oldCodeString'/'$newCodeString'/' gradle.properties

# Version code of Android App Bundle
echo 'Version code of Android App Bundle:' $newCdvVersionCodeValue

# Build Gradle
./gradlew bundleDebug

# Build Gradle Release
./gradlew bundleRelease

# Generate Sign Android App Bundle
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -storepass weppsol -keypass weppsol -keystore ../../../resources/release-key.keystore app/build/outputs/bundle/release/app.aab booking
