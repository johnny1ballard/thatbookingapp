# Inside the git repo
cd thatbookingapp/

# Ionic android build release
ionic cordova build android --prod --aot --minifyjs --minifycss --optimizejs --release -- -- --versionCode=1 -- -- --keystore=../resources/release-key.keystore --alias=booking

# Generate Sign Android APK
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -storepass weppsol -keypass weppsol -keystore ../resources/release-key.keystore platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk booking

# Zip align tool to optimize the APK
zipalign -f -v 4 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk build-doc/play-store/release-apk/Booking.apk

# To verify that your apk is signed run apksigner
apksigner sign --ks-pass pass:weppsol --ks ../resources/release-key.keystore build-doc/play-store/release-apk/Booking.apk
# apksigner verify build-doc/play-store/release-apk/Booking.apk

echo ""
echo "-----------------------Release APK-----------------------------"
echo "./thatbookingapp/build-doc/play-store/release-apk/Booking.apk"
echo "---------------------------------------------------------------"
echo ""
