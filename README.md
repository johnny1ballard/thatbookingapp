# Booking App

Online booking app for Therapist and User.

## Getting Started

The app is providing the iOS & Android platform. User can book their appointement as per there requirement with easily selectable field. Treatment Provider can view all appointement with filters.

### Prerequisites

What things you need to install the software and how to install them

* [nodeJs](https://nodejs.org/en/) - Node.js® is a JavaScript runtime built
* [Ionic](https://ionicframework.com/getting-started#cli) - One codebase. Any platform.
* [IonicMac](https://ionicframework.com/docs/v3/developer-resources/platform-setup/mac-setup.html) - Ionic of Mac setup
* [IonicWindos](https://ionicframework.com/docs/v3/developer-resources/platform-setup/windows-setup.html) - Ionic of Windows setup
* [Android](https://cordova.apache.org/docs/en/8.x/guide/platforms/android/) - Android platform setup
* [AndroidPlayStore](https://ionicframework.com/docs/publishing/play-store) - Android Play store sign APK setup
* [iOS](https://cordova.apache.org/docs/en/8.x/guide/platforms/ios/) - iOS platform setup
* [iOSAppstore](https://ionicframework.com/docs/publishing/app-store) - iOS App store setup


## Steps

Steps for auto build generate using CLI

### Git repo

https
```
git clone https://gitlab.com/wep_sachin/thatbookingapp.git
```
ssh
```
git clone git@gitlab.com:wep_sachin/thatbookingapp.git
```

### Create Branch with prefix hotfix_[id]
```
hotfix_tp467
```

### Update the branch name in script.sh file LN3 & LN9
```
/thatbookingapp/build-doc/script.sh
```

### Update the branch in build.sh file
```
/thatbookingapp/build-doc/build.sh
```
* Update app name - LN2
* Update package name - LN5
* Update color as per section

### Create folder with repo name or any name - hotfix_tp467
```
script.sh file paste inside the folder PATH - hotfix_tp467/script.sh
```
### Create folder of **resources**  inside folder hotfix_tp467 - Name exact same required
```
PATH - hotfix_tp467/resources
```
### Inside the resoures details
* icon.png - 1024 * 1024 must PNG with exact name 
```
PATH - hotfix_tp467/resources/icon.png
```
* splash.png - 2732 * 2732 must PNG with exact name 
```
PATH - hotfix_tp467/resources/splash.png
```
* [ResourcesGuideline](https://ionicframework.com/docs/v3/cli/cordova/resources/) - Resources guide line

## Script RUN
* Past the update script.sh file inside hotfix_tp467 folder
### Inside the branch folder | PATH - hotfix_tp467/script.sh
```
sh script.sh
```



